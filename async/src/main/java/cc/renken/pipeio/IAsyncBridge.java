/**
 * 
 */
package cc.renken.pipeio;

/**
 * @author renkenh
 *
 */
public interface IAsyncBridge<RECV, PUSH> extends IAsyncTube<RECV, RECV, PUSH, PUSH>
{
	//nothing
}
