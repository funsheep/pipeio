/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IComponentContainer;

/**
 * Represents the "end" of the pipeline. It contains/uses some kind of lower-layer protocol (like TCP/IP, RS232, Files, etc.) to communicate with the actual partner.
 * 
 * Therefore, it will never receive data. Instead, data pushed to this component must be send by this component to the actual communication partner.
 * Furthermore, data received through the transportation protocol should be pushed ({@link IComponentContainer#pushToPrevious(Object)}) upwards by this component.
 * 
 * The implementation of the sink can be done in sync with the rest of the pipeline. However, if that is too slow the sink can (of course) run with its own scheduler.
 * However, then unexpected exceptions must be handled by the sink and may be pushed through the {@link IComponentContainer#exceptionEncountered(Exception)} mechanism
 * to the faucet and registered listeners.
 * 
 * @author Hendrik Renken
 */
public interface IAsyncSink<RECV_OUT, PUSH_IN> extends IAsyncComponent<RECV_OUT, Void>
{

	/**
	 * Data that has to be send to the communication partner. The sink has to take care of that. The pipe makes sure, that this method is called only when
	 * the sink is active.
	 * @param entity The data to send to the communication partner.
	 * @throws IOException 
	 * @throws TimeoutException
	 */
	public void push(PUSH_IN entity) throws IOException;
	
	/**
	 * Method default to execute nothing. There are no subsequent (next) components behind a sink. It's the last component of a pipeline.
	 */
	@Override
	public default void activeStateChanged()
	{
		//do nothing - there are no subsequent components that could change
	}

}
