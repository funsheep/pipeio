/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio;

import cc.renken.pipeio.IComponent;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public interface IAsyncComponent<RECV_OUT, PUSH_OUT> extends IComponent<RECV_OUT, PUSH_OUT>
{

	/**
	 * This method is called during activation of the pipeline.
	 * @param handler The handler provides methods to interact with the pipeline and the previous and next component.
	 */
	public void setup(IAsyncComponentContainer<RECV_OUT, PUSH_OUT> handler);

}
