/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncFaucet;
import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;


/**
 * TODO javadoc
 * @author Hendrik Renken
 */
final class AsyncFaucetContainer<RECV, PUSH> extends AAsyncComponentContainer<RECV, Void, Void, PUSH, IAsyncFaucet<RECV, PUSH>>
{


	private final APipeline<RECV, PUSH> pipeline;
	

	public AsyncFaucetContainer(APipeline<RECV,PUSH> pipeline, IAsyncFaucet<RECV, PUSH> faucet, AAsyncComponentContainer<?,RECV,PUSH,?, ? extends IComponent<?,?>> next, IExceptionHandler exceptionHandler)
	{
		super(pipeline.scheduler(), faucet, next, exceptionHandler);
		this.pipeline = pipeline;
		this.component.setup(this);
	}

	
	void activateFaucet(Configuration config) throws IOException, TimeoutException
	{
		super.activate(config);
	}
	
	State getFaucetState()
	{
		return super.getState();
	}
	
	void deactivateFaucet()
	{
		super.deactivate();
	}
	
	@Override
	public void pushToPrevious(Void entity)
	{
		throw new UnsupportedOperationException("Faucet never has a previous handler.");
	}

	@Override
	public void notifyActiveStateChanged()
	{
		if (!this.isHandlerActive())
			throw new IllegalStateException("Container is deactivated.");
		if (this.updateLastState())
		{
			this.component.activeStateChanged();
			try
			{
				this.pipeline.stateChangedNotifyListeners();
			}
			catch (ExecutionException | TimeoutException e)
			{
				AsyncFaucetContainer.this.pipeline.exceptionEncounteredNotifyListeners(e);
			}
		}
	}

	@Override
	public void pushToNext(PUSH entity) throws IOException
	{
		if (!this.isHandlerActive())
			throw new IllegalStateException("Container is deactivated.");
		try
		{
			if (this.getState() != State.ACTIVE)
				throw new IOException("Pipeline is not active");
			this.pipeline.scheduler().waitForExec(() -> super.pushToNext(entity));
			this.pipeline.sendNotifyListeners(convertUnchecked(entity));
		}
		catch (ExecutionException ex)
		{
			AsyncFaucetContainer.this.pipeline.exceptionEncounteredNotifyListeners((Exception) ex.getCause());
			throw new IOException(ex.getCause());
		}
		catch (TimeoutException ex)
		{
			AsyncFaucetContainer.this.pipeline.exceptionEncounteredNotifyListeners(ex);
			throw new IOException(ex);
		}
	}

	@Override
	public State getNextState()
	{
		try
		{
			Callable<State> c = () -> super.getState();
			return this.pipeline.scheduler().waitForExec(c);
		}
		catch (ExecutionException | TimeoutException ex)
		{
			this.pipeline.exceptionEncounteredNotifyListeners(ex);
			if (this.isHandlerActive())
				return State.INACTIVE;
			return State.DEACTIVATED;
		}
	}


	@Override
	protected void pushIn(Void entity) throws IOException
	{
		throw new RuntimeException("Should not happen.");
	}

	@Override
	protected void receiveIn(RECV entity)
	{
		this.component.receive(entity);
		this.pipeline.receivedNotifyListeners(convertUnchecked(entity));
	}

	@SuppressWarnings("unchecked")
	private static <T> T convertUnchecked(Object data)
	{
		return (T) data;
	}

}
