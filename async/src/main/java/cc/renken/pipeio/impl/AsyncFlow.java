/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.util.LinkedHashSet;

import cc.renken.pipeio.IAsyncFaucet;
import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.IPipeline;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public final class AsyncFlow<RECV, PUSH>
{

	public static final class Flowing<RECV_OUT, PUSH_IN, RECV, PUSH>
	{
		
		private final AsyncFlow<RECV, PUSH> asyncFlow;
		

		private Flowing(AsyncFlow<RECV, PUSH> flow)
		{
			this.asyncFlow = flow;
		}

		
		public final <RECV_IN, PUSH_OUT> Flowing<RECV_IN, PUSH_OUT, RECV, PUSH> through(IAsyncTube<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> part)
		{
			this.asyncFlow.parts.add(part);
			return new Flowing<>(this.asyncFlow);
		}
		
		public final Finishing<RECV, PUSH> into(IAsyncSink<RECV_OUT, PUSH_IN> sink)
		{
			this.asyncFlow.asyncSink = sink;
			return new Finishing<>(this.asyncFlow);
		}
	}
	
	public static final class Finishing<RECV, PUSH>
	{
		
		private final AsyncFlow<RECV, PUSH> asyncFlow;

		
		private Finishing(AsyncFlow<RECV, PUSH> flow)
		{
			this.asyncFlow = flow;
		}
		
		public final IPipeline<RECV, PUSH> byId(String id)
		{
			AsyncPipeline<RECV, PUSH> pipe = new AsyncPipeline<>(id, this.asyncFlow.asyncFaucet, this.asyncFlow.parts, this.asyncFlow.asyncSink);
			return pipe;
		}
		
		public final IPipeline<RECV, PUSH> compile()
		{
			AsyncPipeline<RECV, PUSH> pipe = new AsyncPipeline<>(this.asyncFlow.asyncFaucet, this.asyncFlow.parts, this.asyncFlow.asyncSink);
			return pipe;
		}
	}


	private final LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts = new LinkedHashSet<>();
	private final IAsyncFaucet<RECV, PUSH> asyncFaucet;
	private IAsyncSink<?, ?> asyncSink;

	
	private AsyncFlow(IAsyncFaucet<RECV, PUSH> faucet)
	{
		this.asyncFaucet = faucet;
	}


	public static final <RECV, PUSH> Flowing<RECV, PUSH, RECV, PUSH> from(IAsyncFaucet<RECV, PUSH> faucet)
	{
		final AsyncFlow<RECV, PUSH> flow = new AsyncFlow<>(faucet);
		return new Flowing<>(flow);
	}
	
}
