/**
 * 
 */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IScheduler.FailingRunnable;
import cc.renken.pipeio.IScheduler.ITask;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.AtProperties.Property;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
@Property(name=AAsyncSink.PROP_RECOVER, value="4000", validationModel="int:0...", description="The interval used to periodically try to recover the connection after failure.")
public abstract class AAsyncSink<RECV_OUT, PUSH_IN> implements IAsyncSink<RECV_OUT, PUSH_IN>
{
	
	public static final String PROP_RECOVER = "Recover Interval";
	
	
	private final FailingRunnable recoverer = () ->
	{
		if (this.getState() == State.INACTIVE)
		{
			this.disconnect();
			try
			{
				this.connect(this.config);
			}
			catch (IOException ex)
			{
				this.handler.exceptionEncountered(ex);
			}
			this.handler.notifyActiveStateChanged();
		}
	};


	private IAsyncComponentContainer<RECV_OUT, Void> handler;
	private Configuration config;
	
	private ITask watchdog;


	@Override
	public final void setup(IAsyncComponentContainer<RECV_OUT, Void> handler)
	{
		this.handler = handler;
	}

	@Override
	public final void activate(Configuration configuration) throws IOException, TimeoutException
	{
		this.config = configuration;
		int watchInterval = configuration.getIntValue(PROP_RECOVER);
		if (watchInterval > 0)
			this.watchdog = this.handler.getScheduler().schedulePeriodically(this.recoverer, watchInterval, watchInterval, TimeUnit.MILLISECONDS);
		try
		{
			this.connect(this.config);
		}
		catch (IOException | TimeoutException ex)
		{
			this.disconnect();
			throw ex;
		}
		this.handler.notifyActiveStateChanged();
	}


	@Override
	public final State getState()
	{
		if (this.config == null)
			return State.DEACTIVATED;
		if (!this.isConnected())
			return State.INACTIVE;
		return State.ACTIVE;
	}
	
	protected abstract boolean isConnected();

	protected abstract void connect(Configuration configuration) throws IOException, TimeoutException;

	protected abstract void disconnect();
	
	protected final IAsyncComponentContainer<RECV_OUT, Void> handler()
	{
		return this.handler;
	}

	@Override
	public final void deactivate()
	{
		this.disconnect();
		this.config = null;
		if (this.watchdog != null)
		{
			this.watchdog.cancel();
			this.watchdog = null;
		}
	}

}
