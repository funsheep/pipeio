/*
	This file is part of the javaappplatform network library.
	Copyright (C) 2013 funsheep
	Copyright (C) 2005-2013 d3fact Project Team (www.d3fact.de)
	
	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the 
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.pipeio.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cc.renken.pipeio.IAsyncSignal;
import cc.renken.pipeio.IComponentContainer;
import cc.renken.pipeio.IScheduler.ITask;

/**
 * Implementation of the {@link IAsyncSignal} interface. Relies on a {@link IComponentContainer}.
 * @author Hendrik Renken
 */
class AsyncSignal<T> implements IAsyncSignal<T>
{
	
	private final class Waiter
	{
		private ITask task;
		
		private final Callback<T> callback;
		
		public Waiter(Callback<T> callback)
		{
			this.callback = callback;
		}
	}


	private final LinkedList<Waiter> waiters = new LinkedList<>();
	private final IComponentContainer<?, ?> container;


	/**
	 * 
	 */
	AsyncSignal(IComponentContainer<?, ?> container)
	{
		this.container = container;
	}


	@Override
	public void await(Callback<T> callback, T timeoutValue, int timeout, TimeUnit timeUnit)
	{
		Waiter waiter = new Waiter(callback);
		ITask task = this.container.getScheduler().schedule(() -> 
		{
			this.waiters.remove(waiter);
			this.processWaiter(waiter, timeoutValue);
		}, timeout, timeUnit);
		waiter.task = task;
		this.waiters.add(waiter);
	}


	@Override
	public int waiters()
	{
		return this.waiters.size();
	}

	@Override
	public void send(T value)
	{
		Waiter waiter = this.waiters.poll();
		this.processWaiter(waiter, value);
	}

	@Override
	public void sendAll(T value)
	{
		List<Waiter> temp = new ArrayList<>(this.waiters);
		this.waiters.clear();
		for (Waiter waiter : temp)
			this.processWaiter(waiter, value);
	}

	@Override
	public void cancel()
	{
		Waiter waiter = this.waiters.poll();
		if (waiter != null)
			waiter.task.cancel();
	}

	@Override
	public void cancelAll()
	{
		while (this.waiters() > 0)
			this.cancel();
	}
	
	private void processWaiter(Waiter waiter, T value)
	{
		if (waiter != null)
		{
			waiter.task.cancel();
			try
			{
				waiter.callback.call(value);
			}
			catch (Exception ex)
			{
				this.container.exceptionEncountered(ex);
			}
		}
	}

}
