/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncComponent;
import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncSignal;
import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.State;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
abstract class AAsyncComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT, C extends IAsyncComponent<RECV_OUT, PUSH_OUT>> extends AComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT, C> implements IAsyncComponentContainer<RECV_OUT, PUSH_OUT>
{
	
//	private static final Logger logger = LoggerFactory.getLogger(AComponentHandler.class);
	
	/**
	 * 
	 */
	public AAsyncComponentContainer(Scheduler scheduler, C component, AAsyncComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next, IExceptionHandler exceptionHandler)
	{
		super(scheduler, component, next, exceptionHandler);
	}


	@Override
	public <T> IAsyncSignal<T> createSignal()
	{
		return new AsyncSignal<>(this);
	}


	@Override
	protected AAsyncComponentContainer<RECV_OUT, ?, ?, PUSH_IN, ?> previous()
	{
		return (AAsyncComponentContainer<RECV_OUT, ?, ?, PUSH_IN, ?>) super.previous();
	}

	@Override
	protected AAsyncComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next()
	{
		return (AAsyncComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?>) super.next();
	}

	
	protected C getComponent()
	{
		return this.component;
	}

	@Override
	public void pushToPrevious(RECV_OUT entity)
	{
		if (!this.isHandlerActive())
			throw new IllegalStateException("Container is deactivated.");
		this.previous().receiveIn(entity);
	}

	@Override
	public void pushToNext(PUSH_OUT entity) throws IOException, TimeoutException
	{
		assert this.assertIsMe();
		if (!this.isHandlerActive())
			throw new IllegalStateException("Container is deactivated.");
		if (this.getState() != State.ACTIVE)
			throw new IOException("Next component is not active");
		this.next().pushIn(entity);
	}

}
