/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio.impl;

import java.io.IOException;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IExceptionHandler;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
class AsyncSinkContainer<RECV_OUT, PUSH_IN> extends AAsyncComponentContainer<Void, RECV_OUT, PUSH_IN, Void, IAsyncSink<RECV_OUT, PUSH_IN>>
{

	/**
	 * @param pipe
	 * @param previous
	 * @param next
	 */
	public AsyncSinkContainer(Scheduler scheduler, IAsyncSink<RECV_OUT, PUSH_IN> sink, IExceptionHandler exceptionHandler)
	{
		super(scheduler, sink, null, exceptionHandler);
		this.component.setup(this);
	}

	
	@Override
	public void pushToPrevious(RECV_OUT entity)
	{
		if (!this.isHandlerActive())
			throw new IllegalStateException("Container is deactivated.");
		this.getScheduler().submit(() -> super.pushToPrevious(entity));
	}

	@Override
	public void pushToNext(Void entity) throws IOException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	protected void pushIn(PUSH_IN entity) throws IOException
	{
		this.component.push(entity);
	}
	
	@Override
	protected void receiveIn(Void entity)
	{
		throw new UnsupportedOperationException();
	}

}
