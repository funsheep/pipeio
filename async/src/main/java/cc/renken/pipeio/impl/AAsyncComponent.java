/**
 * 
 */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IAsyncComponent;
import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
public abstract class AAsyncComponent<RECV_OUT, PUSH_OUT> implements IAsyncComponent<RECV_OUT, PUSH_OUT>
{
	

	protected IAsyncComponentContainer<RECV_OUT, PUSH_OUT> handler;
	private boolean isActive = false;


	@Override
	public void setup(IAsyncComponentContainer<RECV_OUT, PUSH_OUT> handler)
	{
		this.handler = handler;
	}

	
	@Override
	public void activate(Configuration configuration)
	{
		this.isActive = true;
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	@Override
	public void deactivate()
	{
		this.isActive = false;
	}

}
