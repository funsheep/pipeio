/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This resembles a tube/transformer in a pipe system. Through its methods it can get data from two different directions (incoming, outgoing).
 * See diagram below:
 * 
<pre>
RECV_OUT <---- RECV_IN
PUSH_IN  ----> PUSH_OUT
</pre>
 * 
 * It provides the functionality to transformation the data that flows through it.
 * @author Hendrik Renken
 */
public interface IAsyncTube<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> extends IAsyncComponent<RECV_OUT, PUSH_OUT>
{

	/**
	 * OUT filter method. Used by the pipe to hand an entity over to this {@link IAsyncTube}. This instance is responsible for calling the {@link ITubeHandler#push(Object)} method
	 * to push the same or one or several new entity(s) to the subsequent part of the pipe.
	 * @param entity An entity to process in any way seen fit by this part.
	 */
	public void push(PUSH_IN entity) throws IOException, TimeoutException;

	/**
	 * IN filter method. Used by the pipe to hand an entity over to this {@link IAsyncTube}. This instance is responsible for calling the {@link ITubeHandler#receive(Object)} method
	 * to push the same or one or several new entity(s) to the previous part of the pipe.
	 * This part may process the entity in any way it sees fit, e.g. by updating its active state or by using {@link ITubeHandler#push(Object)} to send another entity.
	 * @param entity An entity to process in any way seen fit by this part.
	 */
	public void receive(RECV_IN entity);

}
