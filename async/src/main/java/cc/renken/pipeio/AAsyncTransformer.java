/**
 * 
 */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.impl.AAsyncComponent;

/**
 * @author renkenh
 *
 */
public abstract class AAsyncTransformer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> extends AAsyncComponent<RECV_OUT, PUSH_OUT> implements IAsyncTube<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT>
{

	
	/**
	 * OUT filter method. Used by the pipe to hand an entity over to this {@link IAsyncTube}. This instance is responsible for calling the {@link ITubeHandler#push(Object)} method
	 * to push the same or one or several new entity(s) to the subsequent part of the pipe.
	 * @param entity An entity to process in any way seen fit by this part.
	 */
	public abstract PUSH_OUT transformPush(PUSH_IN entity);

	/**
	 * IN filter method. Used by the pipe to hand an entity over to this {@link IAsyncTube}. This instance is responsible for calling the {@link ITubeHandler#receive(Object)} method
	 * to push the same or one or several new entity(s) to the previous part of the pipe.
	 * This part may process the entity in any way it sees fit, e.g. by updating its active state or by using {@link ITubeHandler#push(Object)} to send another entity.
	 * @param entity An entity to process in any way seen fit by this part.
	 */
	public abstract RECV_OUT transformReceive(RECV_IN entity);

	
	@Override
	public final void push(PUSH_IN entity) throws IOException, TimeoutException
	{
		PUSH_OUT out = this.transformPush(entity);
		this.handler.pushToNext(out);
	}

	@Override
	public final void receive(RECV_IN entity)
	{
		RECV_OUT out = this.transformReceive(entity);
		this.handler.pushToPrevious(out);
	}

	@Override
	public final void activate(Configuration config)
	{
		//do nothing
	}

	@Override
	public final void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public final State getState()
	{
		return State.ACTIVE;
	}

	@Override
	public final void deactivate()
	{
		//do nothing
	}

}
