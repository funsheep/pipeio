/*
	This file is part of the javaappplatform network library.
	Copyright (C) 2013 funsheep
	Copyright (C) 2005-2013 d3fact Project Team (www.d3fact.de)
	
	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the 
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.pipeio;

import java.util.concurrent.TimeUnit;

/**
 * Convenient concurrency type to create and handle jobs that have a timeout.
 * The signal can be used for single use only. That means, one can create a signal and use it one time. Afterwards one can destroy (or forget) it.
 * 
 *  The second possible usage is, to create one signal and use it several times. In this context, waiters (the callback + timeout) are stored in a list which is
 *  processed (FIFO principle) as soon a timeout hits or send or cancel is used.
 *  The sendAll and cancelAll processes all waiters known at that moment.
 *  
 *  Please note: When using sendAll() it is allowed to create new waiters during the processing of the callbacks. 
 *  
 *  The signal instance can be used again afterwards.
 *  	
 * @author Hendrik Renken
 */
public interface IAsyncSignal<T>
{
	
	@FunctionalInterface
	public interface Callback<T>
	{
		public void call(T value);
	}


	public default void await(Callback<T> callback, int timeout)
	{
		this.await(callback, null, timeout, TimeUnit.MILLISECONDS);
	}

	public void await(Callback<T> callback, T timeoutValue, int timeout, TimeUnit timeUnit);

	public int waiters();
	
	public void send(T value);
	
	public void sendAll(T value);
	
	public void cancel();

	public void cancelAll();

}
