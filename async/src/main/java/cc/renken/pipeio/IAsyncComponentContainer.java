/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This type is the interface that the pipeline provides to the {@link IComponent}. It allows sending data down to the {@link IAsyncSink} by using
 * {@link #pushToNext(Object)} and back up the pipe to the {@link IAsyncFaucet} by using {@link #pushToPrevious(Object)}.
 * The component can ask for the current state of the subsequent pipeline, i.e. if it can send data to the communication partner by querying
 * {@link #getNextState()}.
 * 
 * Scheduling is available by using the scheduler provided by {@link #getScheduler()}.
 * 
 * In contrast to that {@link #notifyActiveStateChanged()} must be called by the component to notify about unexpected changes to its {@link IComponent#getState()}
 * state.
 * Furthermore, to notify about unexpected exceptions that the component cannot handle by itself, it should use {@link #exceptionEncountered(Exception)}
 * to notify the pipeline about that.
 * 
 * @author Hendrik Renken
 */
public interface IAsyncComponentContainer<RECV_OUT, PUSH_OUT> extends IComponentContainer<RECV_OUT, PUSH_OUT>
{
	
	/**
	 * Creates a new signal that can be used for async task completion.
	 * @return A new signal.
	 */
	public <T> IAsyncSignal<T> createSignal();
	
	/**
	 * A component must use this method to push data "up" into the direction of the {@link IAsyncFaucet}. This method can be called at any time
	 * as long as {@link IComponent#activate()} has been called previously, though not from within the {@link IComponent#activate()}
	 * method.
	 * This method may not be called after a call to {@link IComponent#deactivate()}.
	 * 
	 * The previous container then informs its associated component via {@link IAsyncFaucet#receive(Object)}, {@link IAsyncTube#receive(Object)}
	 * method about that entity.
	 * 
	 * @param entity The entity to push upwards.
	 */
	public void pushToPrevious(RECV_OUT entity);
	
	/**
	 * A component must use this method to push data "down" the pipeline to the {@link IAsyncSink}. This method may be called only after a
	 * call to {@link IComponent#activate()} has happened and {@link #getNextState()} returns <code>true</code>.
	 * This method may not be called after a call to {@link IComponent#deactivate()}. 
	 * @param entity The entity to send down.
	 * 
	 * An exception is thrown when somewhere later in the pipeline an exception occurs. The component caller can itself decide what
	 * to do: Catch the exception and handle it (e.g. set my own status to inactive, send an entity up through the pipeline using
	 * {@link #pushToPrevious(Object)}, or do not catch it leaving this tasks to others.
	 * 
	 * @throws IOException is thrown when something happens and the push cannot be finished. E.g. the sink may have no connection to
	 * the communication partner.
	 */
	public void pushToNext(PUSH_OUT entity) throws IOException, TimeoutException;
	
}
