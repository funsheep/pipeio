/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IAsyncComponent;
import cc.renken.pipeio.IAsyncComponentContainer;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class AsyncTestComponent<RECV_OUT, PUSH_OUT> extends ATestComponent<RECV_OUT, PUSH_OUT> implements IAsyncComponent<RECV_OUT, PUSH_OUT>
{

	private IAsyncComponentContainer<RECV_OUT, PUSH_OUT> componentContainer;
	

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setup(IAsyncComponentContainer<RECV_OUT, PUSH_OUT> handler)
	{
		this.componentContainer = handler;
	}
	
	public IAsyncComponentContainer<RECV_OUT, PUSH_OUT> getHandler()
	{
		return this.componentContainer;
	}

	public void setActive(boolean active)
	{
		super.setActive(active);
		this.componentContainer.notifyActiveStateChanged();
	}

}
