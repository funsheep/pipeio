package cc.renken.pipeio.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import cc.renken.pipeio.State;

public class TubeContainerChainTest
{
	
	private final Scheduler scheduler = new Scheduler("Test", "Test", TubeContainerChainTest::handleException)
	{
		@Override
		public boolean isMe()
		{
			return true;
		}
	};
	private final AsyncTestSink<String, String> asyncTestSink = new AsyncTestSink<>();
	private final AsyncSinkContainer<String, String> asyncSinkContainer = new AsyncSinkContainer<>(this.scheduler, asyncTestSink, TubeContainerChainTest::handleException);
	private final AsyncTestTube<String, String> nextTestTube = new AsyncTestTube<>();
	private final AsyncTestTubeContainer<String, String, String, String> nextHandler = new AsyncTestTubeContainer<>(this.scheduler, this.nextTestTube, this.asyncSinkContainer, TubeContainerChainTest::handleException);
	private final AsyncTestTube<String, String> asyncTestTube = new AsyncTestTube<>();
	private final AsyncTestTubeContainer<String, String, String, String> handler = new AsyncTestTubeContainer<>(this.scheduler, this.asyncTestTube, this.nextHandler, TubeContainerChainTest::handleException);
	private final AsyncTestTube<String, String> prevTestTube = new AsyncTestTube<>();
	private final AsyncTestTubeContainer<String, String, String, String> prevHandler = new AsyncTestTubeContainer<>(this.scheduler, this.prevTestTube, this.handler, TubeContainerChainTest::handleException);


	@Test
	public final void testTubeHandler()
	{
		assertEquals(this.handler.component(), this.asyncTestTube);
		assertNotNull(this.handler.getScheduler());
	}

	@Test(expected=IllegalStateException.class)
	public final void testPushInDeactivated() throws Exception
	{
		String toPush = new String();
		this.prevHandler.pushIn(toPush);
	}

	@Test
	public final void testActivateChain() throws Exception
	{
		this.prevHandler.activateContainer();
		assertEquals(State.ACTIVE, this.asyncTestTube.getState());
		assertEquals(State.ACTIVE, this.nextTestTube.getState());
	}
	
	@Test
	public final void testDeactivateChain() throws Exception
	{
		this.prevHandler.deactivateContainer();
		assertEquals(State.DEACTIVATED, this.asyncTestTube.getState());
		assertEquals(State.DEACTIVATED, this.nextTestTube.getState());
	}
	
	@Test
	public final void testPushInChain() throws Exception
	{
		String toPush = new String();
		this.prevHandler.activateContainer();
		this.prevHandler.pushIn(toPush);
		assertEquals(toPush, this.prevTestTube.gotPush());
		assertEquals(toPush, this.asyncTestTube.gotPush());
		assertEquals(toPush, this.nextTestTube.gotPush());
	}

	@Test
	public final void testReceiveInChain() throws Exception
	{
		String toRecv = new String();
		this.prevHandler.activateContainer();
		this.nextHandler.receiveIn(toRecv);
		assertEquals(toRecv, this.prevTestTube.gotRecv());
		assertEquals(toRecv, this.asyncTestTube.gotRecv());
		assertEquals(toRecv, this.nextTestTube.gotRecv());
	}

	@Test
	public final void testIsActiveChain() throws Exception
	{
		this.prevHandler.activateContainer();
		assertEquals(State.ACTIVE, this.handler.getContainerState());
		assertEquals(State.ACTIVE, this.nextHandler.getContainerState());
		this.asyncTestTube.deactivate();
		assertEquals(State.ACTIVE, this.nextHandler.getContainerState());
		assertEquals(State.DEACTIVATED, this.handler.getContainerState());
	}

	@Test
	public final void testHasPrevious()
	{
		assertTrue(this.handler.hasContainerPrevious());
	}

	@Test
	public final void testHasNext()
	{
		assertTrue(this.handler.hasContainerNext());
	}
	
	private static final void handleException(Exception ex)
	{
		ex.printStackTrace();
		Assert.fail(ex.getMessage());
	}
}
