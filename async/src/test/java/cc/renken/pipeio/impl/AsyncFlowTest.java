/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import cc.renken.pipeio.IPipeline;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class AsyncFlowTest
{

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncFlow#through(cc.renken.pipeio.async.ITube)}.
	 */
	@Test
	public final void testById()
	{
		String id = "abc";
		IPipeline<?, ?> pipe = AsyncFlow.from(new TestFaucet<>()).through(new AsyncTestTube<>()).through(new AsyncTestTube<>()).into(new AsyncTestSink<>()).byId(id);
		assertNotNull(pipe);
		assertEquals(id, pipe.id());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncFlow#into(cc.renken.pipeio.async.ISink)}.
	 */
	@Test
	public final void testCompile()
	{
		IPipeline<?, ?> pipe = AsyncFlow.from(new TestFaucet<>()).into(new AsyncTestSink<>()).compile();
		assertNotNull(pipe);
		assertNull(pipe.id());
	}

}
