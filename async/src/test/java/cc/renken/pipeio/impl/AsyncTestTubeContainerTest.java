package cc.renken.pipeio.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import cc.renken.pipeio.State;

public class AsyncTestTubeContainerTest
{
	
	private final Scheduler scheduler = new Scheduler("Test", "Test", AsyncTestTubeContainerTest::handleException)
	{
		@Override
		public boolean isMe()
		{
			return true;
		}
	};
	private final AsyncTestTube<Object, Object> asyncTestTube = new AsyncTestTube<>();
	private final AsyncSinkContainer<Object, Object> asyncSinkContainer = new AsyncSinkContainer<>(scheduler, new AsyncTestSink<>(), AsyncTestTubeContainerTest::handleException);
	private final AsyncTestTubeContainer<Object, Object, Object, Object> handler = new AsyncTestTubeContainer<>(this.scheduler, this.asyncTestTube, this.asyncSinkContainer, AsyncTestTubeContainerTest::handleException);


	@Test
	public final void testPushIn() throws Exception
	{
		Object toPush = new Object();
		this.handler.activateContainer();
		this.handler.pushIn(toPush);
		assertTrue(toPush == this.asyncTestTube.gotPush());
	}

	@Test
	public final void testReceiveIn()
	{
		Object toRecv = new Object();
		this.handler.receiveIn(toRecv);
		assertTrue(toRecv == this.asyncTestTube.gotRecv());
	}

	@Test
	public final void testTubeHandler()
	{
		assertTrue(this.handler.component() == this.asyncTestTube);
		assertTrue(this.handler.getScheduler() != null);
	}

	@Test
	public final void testGetScheduler()
	{
		assertTrue(this.handler.getScheduler() != null);
		assertTrue(this.handler.getScheduler() == this.scheduler);
	}

	@Test
	public final void testIsActive()
	{
		assertEquals(State.DEACTIVATED, this.handler.getContainerState());
	}

	@Test
	public final void testHasPrevious()
	{
		assertFalse(this.handler.hasContainerPrevious());
	}

	@Test
	public final void testHasNext()
	{
		assertTrue(this.handler.hasContainerNext());
	}

	private static final void handleException(Exception ex)
	{
		ex.printStackTrace();
		Assert.fail(ex.getMessage());
	}

}
