/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncFaucet;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class TestFaucet<RECV, PUSH> implements IAsyncFaucet<RECV, PUSH>
{

	private IAsyncComponentContainer<Void, PUSH> componentContainer;
	private boolean isActive = false;

	private final ReentrantLock receivedLock = new ReentrantLock();
	private final Condition waitIsEmpty = this.receivedLock.newCondition();
	private final LinkedList<RECV> received = new LinkedList<>();


	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	@Override
	public void deactivate()
	{
		this.isActive = false;
	}

	@Override
	public void setup(IAsyncComponentContainer<Void, PUSH> handler)
	{
		this.componentContainer = handler;
	}

	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public void receive(RECV entity)
	{
		this.receivedLock.lock();
		try
		{
			this.received.add(entity);
			this.waitIsEmpty.signalAll();
		}
		finally
		{
			this.receivedLock.unlock();
		}
	}


	public RECV pollReceived()
	{
		this.receivedLock.lock();
		try
		{
			if (this.received.isEmpty())
				this.waitIsEmpty.await(1000, TimeUnit.MILLISECONDS);
			if (this.received.isEmpty())
				return null;
			return this.received.removeFirst();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			this.receivedLock.unlock();
		}
	}
	
	public RECV peekReceived()
	{
		this.receivedLock.lock();
		try
		{
			if (this.received.isEmpty())
				this.waitIsEmpty.await(1000, TimeUnit.MILLISECONDS);
			if (this.received.isEmpty())
				return null;
			return this.received.getFirst();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			this.receivedLock.unlock();
		}
	}
	
	public boolean hasReceived()
	{
		this.receivedLock.lock();
		try
		{
			if (this.received.isEmpty())
				this.waitIsEmpty.await(1000, TimeUnit.MILLISECONDS);
			return !this.received.isEmpty();
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			this.receivedLock.unlock();
		}
	}
	
	public void push(PUSH entity) throws IOException, TimeoutException
	{
		this.componentContainer.pushToNext(entity);
	}

}
