/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.ConfigurationBuilder;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class AsyncTestTubeContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> extends AsyncTubeContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT>
{
	
	/**
	 * @param pipe
	 * @param previous
	 * @param next
	 */
	public AsyncTestTubeContainer(Scheduler scheduler, IAsyncTube<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> tube, AAsyncComponentContainer<?, RECV_IN, PUSH_OUT, ?, ? extends IComponent<RECV_IN, ?>> next, IExceptionHandler exceptionHandler)
	{
		super(scheduler, tube, next, exceptionHandler);
		this.component.setup(this);
	}

	
	public void activateContainer() throws IOException, TimeoutException
	{
		super.activate(ConfigurationBuilder.withoutModel().compile());
	}
	
	public State getContainerState()
	{
		return super.getState();
	}
	
	public IAsyncTube<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> component()
	{
		return super.component;
	}
	
	public void deactivateContainer()
	{
		super.deactivate();
	}

	public boolean hasContainerPrevious()
	{
		return super.hasPrevious();
	}
	
	public boolean hasContainerNext()
	{
		return super.hasNext();
	}

}
