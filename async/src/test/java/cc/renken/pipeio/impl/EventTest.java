/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.junit.Test;

import cc.renken.pipeio.IListener.EventType;
import cc.renken.pipeio.config.ConfigurationBuilder;
import cc.renken.pipeio.State;
import cc.renken.pipeio.impl.TestListener.Event;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class EventTest
{

	@Test
	public void testStateChangedEvent() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = AsyncPipelineTest.createChainPipeline();
		TestListener listener = new TestListener();
		pipe.addListener(listener);
		assertEquals(State.DEACTIVATED, pipe.getState());
		assertNull(listener.pollEvent());
		
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, pipe.getState());
		Event e = listener.pollEvent();
		testEvent(EventType.STATE_CHANGED, State.ACTIVE, e);
		
		AsyncTestTube<?,?> tube = ((AsyncTestTube<?, ?>) pipe.getTubes().get(pipe.getTubes().size()-1));

		tube.setActive(false);
		assertEquals(State.INACTIVE, pipe.getState());
		e = listener.pollEvent();
		testEvent(EventType.STATE_CHANGED, State.INACTIVE, e);
		
		tube.setActive(false);
		assertEquals(State.INACTIVE, pipe.getState());
		e = listener.pollEvent();
		assertNull(e);
		
		tube.setActive(true);
		assertEquals(State.ACTIVE, pipe.getState());
		e = listener.pollEvent();
		testEvent(EventType.STATE_CHANGED, State.ACTIVE, e);
		
		tube.setActive(true);
		assertEquals(State.ACTIVE, pipe.getState());
		e = listener.pollEvent();
		assertNull(e);
		
		pipe.deactivate();
		assertEquals(State.DEACTIVATED, pipe.getState());
		e = listener.pollEvent();
		testEvent(EventType.STATE_CHANGED, State.DEACTIVATED, e);
		
		e = listener.pollEvent();
		assertNull(e);
	}
	
	private static final void testEvent(EventType type, Object payload, Event e)
	{
		assertNotNull(e);
		assertEquals(type, e.type);
		assertEquals(payload, e.payload);
	}
	
	
	@Test
	public void exceptionEventTest()
	{
		AsyncTestPipeline<String, String> pipe = ExceptionHandlingTest.createBrokenPipeline();
		BrokenTestTube<?, ?> broken = pipe.getTube(3);
		broken.setBroken(true);
		TestListener listener = new TestListener();
		pipe.addListener(listener);
		try
		{
			pipe.activate(ConfigurationBuilder.withoutModel().compile());
		}
		catch (IOException ex)
		{
			//do nothing
		}
		Event e = listener.pollEvent();
		assertEquals(EventType.EXCEPTION_OCCURRED, e.type);
		assertEquals(RuntimeException.class, e.payload.getClass());
	}
	
}
