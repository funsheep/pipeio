package cc.renken.pipeio.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IAsyncTube;

public class AsyncTestPipeline<RECV, PUSH> extends AsyncPipeline<RECV, PUSH>
{
	
	private final TestFaucet<RECV,PUSH> faucet;
	private final List<IAsyncTube<?, ?, ?, ?>> parts;
	private final IAsyncSink<?, ?> asyncSink;
	private TestScheduler scheduler;


	public AsyncTestPipeline(TestFaucet<RECV, PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		super(faucet, parts, sink);
		this.faucet = faucet;
		this.parts = new ArrayList<>(parts);
		this.asyncSink = sink;
	}

	
	public TestFaucet<RECV,PUSH> getFaucet()
	{
		return this.faucet;
	}
	
	public List<IAsyncTube<?,?,?,?>> getTubes()
	{
		return this.parts;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends IAsyncTube<?, ?, ?, ?>> T getTube(int index)
	{
		return (T) this.parts.get(index);
	}
	
	public IAsyncSink<?,?> getSink()
	{
		return this.asyncSink;
	}

	@Override
	public Scheduler scheduler()
	{
		if (this.scheduler == null)
			this.scheduler = new TestScheduler(this::exceptionEncounteredNotifyListeners);
		return this.scheduler;
	}

}
