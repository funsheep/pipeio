/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;

import org.junit.Test;

import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.ConfigurationBuilder;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class ExceptionHandlingTest
{

	public static final AsyncTestPipeline<String, String> createBrokenPipeline()
	{
		LinkedHashSet<IAsyncTube<?, ?, ?, ?>> set = new LinkedHashSet<>();
		AsyncTestTube<String, String> nextTestTube = new AsyncTestTube<>();
		AsyncTestTube<String, String> testTube = new AsyncTestTube<>();
		AsyncTestTube<String, String> prevTestTube = new AsyncTestTube<>();
		BrokenTestTube<String, String> brokenTube = new BrokenTestTube<>();
		set.addAll(Arrays.asList(prevTestTube, testTube, nextTestTube, brokenTube));
		return new AsyncTestPipeline<>(new TestFaucet<>(), set, new AsyncTestSink<>());
	}
	
	
	@Test
	public final void testActiveException() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createBrokenPipeline();
		BrokenTestTube<?, ?> broken = pipe.getTube(3);
		broken.setBroken(true);
		boolean exceptionThrown = false;
		try
		{
			pipe.activate(ConfigurationBuilder.withoutModel().compile());
		}
		catch (IOException ex)
		{
			assertEquals("Failing in activate()", ex.getCause().getMessage());
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
		assertEquals(State.DEACTIVATED, pipe.getState());
	}

	@Test
	public final void testPushException() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createBrokenPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());

		BrokenTestTube<?, ?> broken = pipe.getTube(3);
		broken.setBroken(true);
		boolean exceptionThrown = false;
		try
		{
			pipe.getFaucet().push("Hello");
		}
		catch (IOException ex)
		{
			assertEquals(IOException.class, ex.getClass());
			assertEquals("Failing in push()", ex.getCause().getMessage());
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
		assertEquals(State.ACTIVE, pipe.getState());
	}

	@Test
	public final void testDeactivateException() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createBrokenPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		pipe.getFaucet().push("Hello");

		BrokenTestTube<?, ?> broken = pipe.getTube(3);
		broken.setBroken(true);
		boolean exceptionThrown = false;
		try
		{
			pipe.deactivate();
		}
		catch (IOException ex)
		{
			assertEquals("Failing in deactivate()", ex.getCause().getMessage());
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
		assertEquals(State.DEACTIVATED, pipe.getState());
		assertEquals(State.ACTIVE, pipe.getSink().getState());
	}

}
