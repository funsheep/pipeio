/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class BrokenTestTube<RECV, PUSH> implements IAsyncTube<RECV, RECV, PUSH, PUSH>
{

	private boolean isBroken = false;
	private boolean isActive = false;
	private IAsyncComponentContainer<RECV, PUSH> componentContainer;
	
	
	@Override
	public void setup(IAsyncComponentContainer<RECV, PUSH> handler)
	{
		this.componentContainer = handler;
	}


	@Override
	public void activate(Configuration config)
	{
		if (this.isBroken)
			throw new RuntimeException("Failing in activate()");
		this.isActive = true;
	}

	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	@Override
	public void deactivate()
	{
		if (this.isBroken)
			throw new RuntimeException("Failing in deactivate()");
		this.isActive = false;
	}

	@Override
	public void push(PUSH entity) throws IOException, TimeoutException
	{
		if (this.isBroken)
			throw new IOException("Failing in push()");
		this.componentContainer.pushToNext(entity);
	}

	@Override
	public void receive(RECV entity)
	{
		this.componentContainer.pushToPrevious(entity);
	}

	public void setBroken(boolean isBroken)
	{
		this.isBroken = isBroken;
	}

}
