/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.IListener.EventType;
import cc.renken.pipeio.config.ConfigurationBuilder;
import cc.renken.pipeio.State;
import cc.renken.pipeio.impl.TestListener.Event;


/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class AsyncPipelineTest
{

	public static final AsyncTestPipeline<String, String> createChainPipeline()
	{
		LinkedHashSet<IAsyncTube<?, ?, ?, ?>> set = new LinkedHashSet<>();
		AsyncTestTube<String, String> nextTestTube = new AsyncTestTube<>();
		AsyncTestTube<String, String> testTube = new AsyncTestTube<>();
		AsyncTestTube<String, String> prevTestTube = new AsyncTestTube<>();
		set.addAll(Arrays.asList(prevTestTube, testTube, nextTestTube));
		return new AsyncTestPipeline<>(new TestFaucet<>(), set, new AsyncTestSink<>());
	}
	
	public static final AsyncTestPipeline<String, String> createOnlySinkPipeline()
	{
		return new AsyncTestPipeline<>(new TestFaucet<>(), new LinkedHashSet<>(), new AsyncTestSink<>());
	}


	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#Pipeline(java.util.LinkedHashSet, cc.renken.pipeio.async.ISink)}.
	 */
	@Test
	public final void testCreatePipeline()
	{
		assertNotNull(createChainPipeline());
		assertNotNull(createOnlySinkPipeline());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#activate()}.
	 */
	@Test
	public final void testDeActivate() throws Exception
	{
		testDeActivate(createChainPipeline());
		testDeActivate(createOnlySinkPipeline());
	}
	
	private static final void testDeActivate(AsyncPipeline<?, ?> pipe) throws Exception
	{
		assertEquals(State.DEACTIVATED, pipe.getState());
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, pipe.getState());
		pipe.deactivate();
		assertEquals(State.DEACTIVATED, pipe.getState());
//		pipe.shutdown();
//		assertEquals(pipe.getState(), State.DEACTIVATED);
	}
	
	@Test
	public final void testTimesDeActivate() throws Exception
	{
		AsyncTestPipeline<String, String> pipeline = createChainPipeline();
		for (int i = 0; i < 100; i++)
			testDeActivate(pipeline);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public final void testHalfActivate() throws Exception
	{
		AsyncTestPipeline<String, String> pipeline = createChainPipeline();
		pipeline.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, pipeline.getState());
		((AsyncTestTube<String, String>) pipeline.getTubes().get(2)).setActive(false);
		assertEquals(State.INACTIVE, pipeline.getState());
		((AsyncTestTube<String, String>) pipeline.getTubes().get(1)).setActive(false);
		assertEquals(State.INACTIVE, pipeline.getState());
		assertTrue(((AsyncTestTube<String, String>) pipeline.getTubes().get(0)).gotActiveStateChanged());
		pipeline.activate(ConfigurationBuilder.withoutModel().compile());
		Thread.sleep(1000);
		assertEquals(State.INACTIVE, pipeline.getState());
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public final void testHalfDeactivate() throws Exception
	{
		AsyncTestPipeline<String, String> pipeline = createChainPipeline();
		pipeline.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, pipeline.getState());
		((AsyncTestTube<String, String>) pipeline.getTubes().get(2)).setActive(false);
		pipeline.deactivate();
		Thread.sleep(1000);
		assertEquals(State.DEACTIVATED, pipeline.getState());
		for (IAsyncTube<?,?,?,?> tube : pipeline.getTubes())
			assertEquals(State.DEACTIVATED, tube.getState());
	}
	
	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#pushEntity(java.lang.Object)}.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public final void testPushEntity() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		pipe.getFaucet().push("Hello");
		AsyncTestSink<String, String> sink = (AsyncTestSink<String, String>) pipe.getSink();
		assertEquals("Hello", sink.gotPush());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#hasReceivedEntities()}.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public final void testHasReceivedEntities() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		AsyncTestSink<String, String> sink = (AsyncTestSink<String, String>) pipe.getSink();
		sink.receive("Hello");
		assertTrue(pipe.getFaucet().hasReceived());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#pollEntity()}.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public final void testPollEntity() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		AsyncTestSink<String, String> sink = (AsyncTestSink<String, String>) pipe.getSink();
		sink.receive("Hello");
		assertEquals("Hello", pipe.getFaucet().pollReceived());
		sink.receive("World");
		assertEquals("World", pipe.getFaucet().pollReceived());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#pollEntity()}.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public final void testPeekEntity() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		AsyncTestSink<String, String> sink = (AsyncTestSink<String, String>) pipe.getSink();
		sink.receive("Hello");
		assertEquals("Hello", pipe.getFaucet().peekReceived());
		sink.receive("World");
		assertEquals("Hello", pipe.getFaucet().pollReceived());
		assertEquals("World", pipe.getFaucet().pollReceived());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#pollEntity(long)}.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public final void testPollEntityLong() throws Exception
	{
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		AsyncTestSink<String, String> sink = (AsyncTestSink<String, String>) pipe.getSink();
		pipe.scheduler().schedule(() -> sink.receive("Hello"), 10, TimeUnit.MILLISECONDS);
		assertEquals("Hello", pipe.getFaucet().pollReceived());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.EventNotifier#addListener(cc.renken.pipeio.IListener)}.
	 */
	@Test
	public final void testAddRemoveListenerPipeline()
	{
		TestListener listener = new TestListener();
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.addListener(listener);
		pipe.removeListener(listener);
		pipe.removeListener(listener);
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.EventNotifier#stateChangedNotifyListeners()}.
	 */
	@Test
	public final void testStateChangedNotifyConsumers() throws IOException
	{
		TestListener listener = new TestListener();
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.addListener(listener);
		
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		Event e = listener.pollEvent();
		assertNotNull(e);
		assertEquals(EventType.STATE_CHANGED, e.type);
		assertEquals(State.ACTIVE, e.payload);
		assertNull(listener.pollEvent());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.EventNotifier#receivedNotifyConsumers()}.
	 */
	@Test
	public final void testReceivedNotifyConsumers()
	{
		TestListener listener = new TestListener();
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.addListener(listener);
		
		pipe.receivedNotifyListeners("Hello");
		Event e = listener.pollEvent();
		assertEquals(EventType.DATA_RECEIVED, e.type);
		assertEquals("Hello", e.payload);
		assertNull(listener.pollEvent());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AsyncPipeline#exceptionEncounteredNotifyListeners(java.lang.Exception)}.
	 */
	@Test
	public final void testExceptionEncounteredNotifyConsumers() throws Exception
	{
		TestListener listener = new TestListener();
		AsyncTestPipeline<String, String> pipe = createChainPipeline();
		pipe.addListener(listener);

		Exception ex = new Exception();
		pipe.exceptionEncounteredNotifyListeners(ex);
		Event e = listener.pollEvent();
		assertEquals(EventType.EXCEPTION_OCCURRED, e.type);
		assertEquals(ex, e.payload);
	}

}
