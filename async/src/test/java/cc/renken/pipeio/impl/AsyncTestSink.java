/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IAsyncSink;

/**
 * TODO javadoc
 * 
 * @author Hendrik Renken
 */
public class AsyncTestSink<RECV, PUSH> extends AsyncTestComponent<RECV, Void> implements IAsyncSink<RECV, PUSH>
{

	private PUSH pushed = null;
	

	@Override
	public void push(PUSH entity)
	{
		this.pushed = entity;
	}
	
	public PUSH gotPush()
	{
		return this.pushed;
	}

	public void receive(RECV entity)
	{
		this.pushed = null;
		this.getHandler().pushToPrevious(entity);
	}

}
