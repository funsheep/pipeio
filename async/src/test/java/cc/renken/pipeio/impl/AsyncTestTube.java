/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncTube;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class AsyncTestTube<RECV, PUSH> extends AsyncTestComponent<RECV, PUSH> implements IAsyncTube<RECV, RECV, PUSH, PUSH>
{
	
	private PUSH gotPush;
	private RECV gotRecv;


	@Override
	public void push(PUSH entity) throws IOException, TimeoutException
	{
		this.gotPush = entity;
		this.getHandler().pushToNext(entity);
	}
	
	public PUSH gotPush()
	{
		return this.gotPush;
	}

	@Override
	public void receive(RECV entity)
	{
		this.gotRecv = entity;
		if (((AsyncTubeContainer<?,?,?,?>) this.getHandler()).hasPrevious())
			this.getHandler().pushToPrevious(entity);
	}

	public RECV gotRecv()
	{
		return this.gotRecv;
	}

}
