/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.async;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.IPipeline;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.ConfigurationBuilder;
import cc.renken.pipeio.impl.AsyncFlow;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class SimpleAsyncDemo
{

	
	public static final void main(String[] args) throws Exception
	{
		StringGenerator faucet = new StringGenerator();
		IPipeline<String, String> pipe = AsyncFlow.from(faucet).through(new StringObjectTransformer()).through(new LoggingTube<>()).into(new EchoSink()).compile();
		pipe.addListener(new IListener()
		{
			
			@Override
			public void handleEvent(EventType type, Object entity)
			{
				switch (type)
				{
					case DATA_RECEIVED:
						System.out.println(entity);
						break;
					case EXCEPTION_OCCURRED:
						((Exception) entity).printStackTrace();
						break;
					case STATE_CHANGED:
						System.out.println(entity);
						break;
					default:
						break;
					
				}
			}
		});
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
		Runnable call = () ->
		{
			try
			{
				if (pipe.getState() == State.ACTIVE)
				{
					System.out.println("Pushed");
					faucet.push("AAAA");
				}
			}
			catch (IOException | TimeoutException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		service.scheduleAtFixedRate(call, 1, 1, TimeUnit.SECONDS);
	}

}
