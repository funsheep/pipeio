/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.async;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class HealthCheck implements IAsyncTube<String, String, String, String>
{

	public static final String HEALTH_CHECK = "I'm alive!";
	
	private IAsyncComponentContainer<String, String> componentContainer;
	private Instant lastRecv = Instant.now();
	private Instant lastSend = Instant.now();
	private boolean isAlive = true;
	private boolean isActive = false;

	@Override
	public void setup(IAsyncComponentContainer<String, String> handler)
	{
		this.componentContainer = handler;
	}

	@Override
	public void activate(Configuration config)
	{
		System.out.println("Healthcheck activated");
		//do this in faucet
		this.isActive = true;
	}
	
	private void checkHealth()
	{
		this.isAlive = ChronoUnit.SECONDS.between(this.lastSend, this.lastRecv) <= 1;
		System.out.println(this.getState());
		this.componentContainer.notifyActiveStateChanged();
	}

	@Override
	public void activeStateChanged()
	{
		this.componentContainer.notifyActiveStateChanged();
	}

	@Override
	public void deactivate()
	{
		//do nothing
		this.isActive = false;
	}

	@Override
	public void push(String entity) throws IOException, TimeoutException
	{
		this.componentContainer.pushToNext(entity);
		if (HEALTH_CHECK.equals(entity))
		{
			this.lastSend = Instant.now();
			System.out.println("Pushed Health");
			this.componentContainer.getScheduler().schedule(this::checkHealth, 1, TimeUnit.SECONDS);
		}
	}

	@Override
	public void receive(String entity)
	{
		if (HEALTH_CHECK.equals(entity))
		{
			System.out.println("Recv Health");
			this.lastRecv = Instant.now();
			return;
		}
		this.componentContainer.pushToPrevious(entity);
	}

	@Override
	public State getState()
	{
		if (!this.isActive)
			return State.DEACTIVATED;
		if (this.isAlive)
			return State.ACTIVE;
		return State.INACTIVE;
	}

}
