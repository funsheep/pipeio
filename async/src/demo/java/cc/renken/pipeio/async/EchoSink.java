/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.async;

import java.io.IOException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class EchoSink implements IAsyncSink<Object, Object>
{

	private IAsyncComponentContainer<Object, Void> componentContainer;
	private boolean isActive = false;

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setup(IAsyncComponentContainer<Object, Void> handler)
	{
		this.componentContainer = handler;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(Object entity) throws IOException
	{
		if (!this.isActive)
			throw new IOException("Sink not ready.");
		this.componentContainer.pushToPrevious(entity);
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deactivate()
	{
		this.isActive = false;
	}


	@Override
	public void activeStateChanged()
	{
		// TODO Auto-generated method stub
		
	}

}
