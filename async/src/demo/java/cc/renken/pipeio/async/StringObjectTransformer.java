/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.async;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class StringObjectTransformer implements IAsyncTube<Object, String, String, Object>
{
	
	private IAsyncComponentContainer<String, Object> componentContainer;
	private boolean isActive = false;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setup(IAsyncComponentContainer<String, Object> handler)
	{
		this.componentContainer = handler;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deactivate()
	{
		this.isActive = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(String entity) throws IOException, TimeoutException
	{
		if (!this.isActive)
			throw new IOException("Transformer not ready.");
		this.componentContainer.pushToNext(entity);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void receive(Object entity)
	{
		this.componentContainer.pushToPrevious(String.valueOf(entity));
	}

}
