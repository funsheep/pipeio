/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.async;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class LoggingTube<RECV, PUSH> implements IAsyncTube<RECV, RECV, PUSH, PUSH>
{
	
	private static final Logger logger = LoggerFactory.getLogger(LoggingTube.class);


	private IAsyncComponentContainer<RECV, PUSH> componentContainer;
	private boolean isActive = false;

	
	@Override
	public void setup(IAsyncComponentContainer<RECV, PUSH> handler)
	{
		this.componentContainer = handler;
	}


	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	@Override
	public void deactivate()
	{
		this.isActive = false;
	}

	@Override
	public void push(PUSH entity) throws IOException, TimeoutException
	{
		if (!this.isActive)
			throw new IOException("LoggingTube not ready");
		logger.info("Push Entity: {}", entity);
		this.componentContainer.pushToNext(entity);
	}

	@Override
	public void receive(RECV entity)
	{
		logger.info("Receive Entity: {}", entity);
		this.componentContainer.pushToPrevious(entity);
	}


	@Override
	public void activeStateChanged()
	{
		logger.info("State of subpipe changed: {}", this.componentContainer.getNextState());
	}


}
