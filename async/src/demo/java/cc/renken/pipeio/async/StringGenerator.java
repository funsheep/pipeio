/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.async;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncFaucet;
import cc.renken.pipeio.IScheduler.ITask;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.State;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class StringGenerator implements IAsyncFaucet<String, String>
{

	private IAsyncComponentContainer<Void, String> componentContainer;
	private ITask task;


	@Override
	public void setup(IAsyncComponentContainer<Void, String> handler)
	{
		this.componentContainer = handler;
	}

	@Override
	public void activate(Configuration config)
	{
		if (this.task != null)
			return;
		this.task = this.componentContainer.getScheduler().schedulePeriodically(this::pushHealthCheck, 1, 10, TimeUnit.SECONDS);
	}
	
  	public void push(String entity) throws IOException, TimeoutException
	{
		this.componentContainer.pushToNext(entity);
	}
	
	private void pushHealthCheck()
	{
		if (this.componentContainer.getNextState() == State.ACTIVE)
		{
			try
			{
				this.componentContainer.pushToNext(HealthCheck.HEALTH_CHECK);
			}
			catch (IOException | TimeoutException e)
			{
				this.componentContainer.exceptionEncountered(e);
			}
		}
	}
	
	@Override
	public void activeStateChanged()
	{
		System.out.println("State changed");
		if (this.componentContainer.getNextState() == State.ACTIVE && this.task == null)
			this.activate(null);
		else if (this.componentContainer.getNextState() != State.ACTIVE && this.task != null)
			this.deactivate();
	}

	@Override
	public void deactivate()
	{
		if (this.task != null)
		{
			this.task.cancel();
			assert this.task.isCanceled();
			this.task = null;
		}
	}

	@Override
	public void receive(String entity)
	{
		System.out.println("Faucet received " + entity);
	}

	@Override
	public State getState()
	{
		if (this.task != null)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

}
