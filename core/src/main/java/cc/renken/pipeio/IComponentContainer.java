/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This type is the interface that the pipeline provides to the {@link IComponent}. It allows sending data down to the {@link ISink} by using
 * {@link #pushToNext(Object)} and back up the pipe to the {@link IFaucet} by using {@link #pushToPrevious(Object)}.
 * The component can ask for the current state of the subsequent pipeline, i.e. if it can send data to the communication partner by querying
 * {@link #getNextState()}.
 * 
 * Scheduling is available by using the scheduler provided by {@link #getScheduler()}.
 * 
 * In contrast to that {@link #notifyActiveStateChanged()} must be called by the component to notify about unexpected changes to its {@link IComponent#getState()}
 * state.
 * Furthermore, to notify about unexpected exceptions that the component cannot handle by itself, it should use {@link #exceptionEncountered(Exception)}
 * to notify the pipeline about that.
 * 
 * @author Hendrik Renken
 */
public interface IComponentContainer<RECV_OUT, PUSH_OUT>
{
	
	public void activateNext() throws IOException, TimeoutException;
	
	/**
	 * Must be used by a {@link IComponent} to notify about an {@link IComponent#getState()} state change. This method can be called at
	 * any time as long as {@link IComponent#activate()} has been called previously, though not from within the {@link IComponent#activate()}
	 * method. This method may not be called after a call to {@link IComponent#deactivate()}.
	 * 
	 * The component must not in include (or respect) active states from previous or next components. This method is for changes to
	 * its own active/inactive-state only. For example, a health check component is inactive when the health check fails, regardless
	 * whether the next component is active or inactive (though it is of course inactive when the next component is inactive as it
	 * is not possible to check the health status then - which means it fails). Another example: A simple transformer is active and
	 * is able to transform data even when the next component is inactive.
	 * 
	 * @param active Whether it considers itself active (let entity pass through) or not.
	 */
	public void notifyActiveStateChanged();

	/**
	 * Returns whether the subsequent pipeline is active(), this especially includes all subsequent (next) components. I.E. if from several
	 * components as least one component is inactive ({@link IComponent#getState()} returns <code>false</code>) this method will return <code>false</code>.
	 * @return Returns whether {@link #pushToNext(Object)} is allowed or not.
	 */
	public State getNextState();

	public void deactivateNext();
	
	/**
	 * Returns a scheduler that provides methods for scheduling tasks. This method can be called at any time as long as 
	 * {@link IComponent#activate()} has been called previously, though not from within the {@link IComponent#activate()} method.
	 * This method may not be called after a call to {@link IComponent#deactivate()}.
	 */
	public IScheduler getScheduler();

	/**
	 * Informs the pipeline that an unexpected exception within the component has happened. The pipeline then can take actions like disabling
	 * the single component, etc. Furthermore, the exception is passed to the {@link IListener}s registered at the pipeline.
	 * @param ex The exception that has unexpectedly occurred.
	 */
	public void exceptionEncountered(Exception ex);

}
