/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.config.AtProperties.Property;
import cc.renken.pipeio.config.Configuration;

/**
 * This type specifies a component in the pipeline. The component is embedded into a container that is provided by the pipeline.
 * This ensures a correct working main logic. 
 * 
 * The component has methods that are used by the pipeline to control its state. All methods, except the shutdown() method are called
 * from the same thread.
 * 
 * The {@link #setup(IComponentContainer)} method is called when the pipeline is setup as a whole. The setup happens from start to end, i.e. after calling this
 * method, the next component (accessible through the handler) is not available just yet.
 * 
 * {@link #activate()} and {@link #deactivate()} handle the life cycle of the component. Access to methods to the {@link IComponentContainer}
 * should be done only after a call to {@link #activate()} and before any call to {@link #deactivate()}. This includes the
 * scheduler methods.
 * 
 * A component represents a part of the pipeline. Derived types have part-specific methods to receive data for processing. To send data
 * along the pipeline the component has to use the methods of the handler. Using the scheduling mechanism a component can e.g. periodically
 * send data into the pipeline. The methods for sending data are {@link IComponentContainer#pushToNext(Object)} and {@link IComponentContainer#pushToPrevious(Object)}.
 * 
 * The pipeline does not manage/follow up on the {@link #getState()} state of the component by itself. Instead, the component is responsible by
 * any unexpected! state change to call {@link IComponentContainer#notifyActiveStateChanged()}. Unexpected means, that it does not have to call this method
 * when {@link #activate()} or {@link #deactivate()} is called.
 * 
 * Every component works for itself and must not rely on the states and events of others. E.g. the {@link #getState()} method should consider for
 * its return value the current state of this component only. It does not matter whether it is able to work or not (i.e. the subsequent pipeline is
 * deactivated). Furthermore, receiving data from the next component in line is always possible - e.g. to update health information or react to
 * an incoming "establish connection" message. 
 * 
 * @author Hendrik Renken
 */
@Property(name=IComponent.PROP_TIMEOUT, isRequired=true, validationModel="int:1...", value="10000", description="Timeout in Milliseconds.")
public interface IComponent<RECV_OUT, PUSH_OUT>
{

	public static final String PROP_TIMEOUT = "timeout";

	/**
	 * This method is called during activation of the pipeline. Before this instance is activated (i.e. the method is called)
	 * the next component (in the pipeline, if any) is activated first.
	 */
	public void activate(Configuration configuration) throws IOException, TimeoutException;

	/**
	 * Trigger method. It is used by the pipeline to inform this component that the state of the subsequent (next) parts
	 * has changed, e.g. because the health check was unsuccessful or a lost connection was reestablished again.
	 */
	public void activeStateChanged();	//FIXME move this into faucet and component (check complexity before)

	/**
	 * Returns whether this component (independent of the other components) is active or not. This state does not depend on the state of others.
	 * That means - if this component has been {@link #activate()} and in the meantime has not been {@link #deactivate()}d - and there is also no
	 * other reason why this component can't work - it must return {@link State#ACTIVE}. E.g. a transformer or a connection check is active, even if the subsequent
	 * pipeline is inactive, i.e. {@link IComponentContainer#getNextState()} returns {@link State#DEACTIVATED}. However, a health check that has failed should return
	 * {@link State#INACTIVE}. Even if it still constantly checks the health state. 
	 * @return The current state of the component.
	 */
	public State getState();
	
	/**
	 * Should deactivate this component, i.e. stop any scheduled tasks //FIXME add this logic to the handler - check and stop all tasks of this component after calling this method
	 * Free any resources, e.g. clear any data buffer. 
	 * The component must return {@link State#DEACTIVATED} for {@link #getState()} after calling this method.
	 */
	public void deactivate();

//	public void shutdown();
}
