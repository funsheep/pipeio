/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import cc.renken.pipeio.config.IPropertyValidator.ValueType;
import cc.renken.pipeio.config.PropertyModel.Property;
import cc.renken.pipeio.util.EnumUtils;


/**
 * 
 * @author renkenh
 */
public final class Configuration
{
	
	private final Map<String, String> config;
	private final PropertyModel model;
	

	Configuration(Map<String, String> config)
	{
		this(config, null);
	}

	Configuration(Map<String, String> config, PropertyModel model)
	{
		this.config = new HashMap<>(config);
		this.model  = model;
	}



	public final String getStringValue(String key)
	{
		String value = this.config.get(key);
		if (value == null && this.model != null)
			value = this.model.getProperty(key).defaultValue();
		return value;
	}

	public final int getIntValue(String key)
	{
		String value = this.getStringValue(key);
		return Integer.parseInt(value);
	}

	public final float getFloatValue(String key)
	{
		String value = this.getStringValue(key);
		return Float.parseFloat(value);
	}

	public final boolean getBooleanValue(String key)
	{
		String value = this.getStringValue(key);
		return Boolean.parseBoolean(value);
	}

	@SuppressWarnings("unchecked")
	public <E extends Enum<E>> E getEnumEntry(String key)
	{
		if (this.model == null)
			throw new IllegalStateException("This method is usable with a defined property model only.");
		String value = this.getStringValue(key);
		Property property = this.model.getProperty(key);
		if (property.valueType() != ValueType.ENUM)
			throw new IllegalArgumentException("Given key does not refer to an enum value.");
		
		for (Object v : EnumUtils.getEnumValues(property.<EnumValidator>getValidator().enumClass()))
		{
			if (String.valueOf(v).equalsIgnoreCase(value.toLowerCase()))
				return (E) v;
		}
		throw new NoSuchElementException("Key "+key+" does not have an enum value.");
	}

	public Map<String, String> getMap()
	{
		return Collections.unmodifiableMap(this.config);
	}
}
