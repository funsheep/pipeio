package cc.renken.pipeio.config;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public interface IPropertyValidator
{
	
	public enum ValueType
	{
		STRING, INT, FLOAT, BOOLEAN, ENUM
	}

	public enum ValidatorType
	{
		INTERVAL, LIST, CUSTOM
	}
	
	
	@Documented
	@Retention(RUNTIME)
	@Target(TYPE)
	public @interface AtValidatorConfigs
	{
		/**
		 * @return All specified proprties.
		 */
		public Validator[] value();
		
		/**
		 * Annotation that specifies properties, their default value (if any), description and validation.
		 */
		@Retention(RetentionPolicy.RUNTIME)
		@Target(ElementType.TYPE)
		@Repeatable(AtValidatorConfigs.class)
		public @interface Validator
		{
			
			public String value();
		}

	}


	public default ValidatorType getValidatorType()
	{
		return ValidatorType.CUSTOM;
	}
	
	public ValueType getValueType();

	public String validate(String value2Validate);

}
