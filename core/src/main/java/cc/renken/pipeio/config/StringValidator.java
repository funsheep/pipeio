/**
 * 
 */
package cc.renken.pipeio.config;

/**
 * @author renkenh
 *
 */
public class StringValidator extends AValidator<String>
{

	/**
	 * @param validationSpecs
	 */
	public StringValidator(String validationSpecs)
	{
		super(validationSpecs);
	}


	@Override
	public ValueType getValueType()
	{
		return ValueType.STRING;
	}


	@Override
	protected String convertSpec(String word)
	{
		return word;
	}

}
