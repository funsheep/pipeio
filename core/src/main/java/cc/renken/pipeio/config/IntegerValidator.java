/**
 * 
 */
package cc.renken.pipeio.config;

import cc.renken.pipeio.config.IPropertyValidator.AtValidatorConfigs.Validator;

/**
 * @author renkenh
 *
 */
@Validator("int")
public class IntegerValidator extends AValidator<Integer>
{

	/**
	 * @param validationSpecs
	 */
	public IntegerValidator(String validationSpecs)
	{
		super(validationSpecs);
	}


	@Override
	public ValueType getValueType()
	{
		return ValueType.INT;
	}

	@Override
	protected Integer convertSpec(String word)
	{
		return Integer.valueOf(word);
	}

}
