/**
 * 
 */
package cc.renken.pipeio.config;

import cc.renken.pipeio.config.IPropertyValidator.AtValidatorConfigs.Validator;

/**
 * @author renkenh
 *
 */
@Validator("float")
public class FloatValidator extends AValidator<Float>
{

	/**
	 * @param validationSpecs
	 */
	public FloatValidator(String validationSpecs)
	{
		super(validationSpecs);
	}


	@Override
	public ValueType getValueType()
	{
		return ValueType.FLOAT;
	}

	@Override
	protected Float convertSpec(String word)
	{
		return Float.valueOf(word);
	}

}
