/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.List;

/**
 * This validator interface can be used as a front end to display a list of valid values.
 * Must be implemented by any {@link IPropertyValidator} that returns {@link StandardValidatorType#LIST}
 * on {@link #getInputType()}.
 * @author renkenh
 */
public interface IListValidator<TYPE> extends IPropertyValidator
{
	
	public static final String ERROR_TEXT = "Given value is not within the set of valid values.";


	@Override
	public default ValidatorType getValidatorType()
	{
		return ValidatorType.LIST;
	}

	/**
	 * @return The list of valid values. Unmodifiable. Can be used to display them on an UI.
	 */
	public List<TYPE> getValidValues();

}
