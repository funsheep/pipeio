/**
 * 
 */
package cc.renken.pipeio.config;

/**
 * This validator interface can be used as a front end to display a range of valid values.
 * Must be implemented by any {@link IPropertyValidator} that returns {@link StandardValidatorType#FLOAT}
 * on {@link #getInputType()}.
 * The implementation must guarantee that {@link #getLowerBound()} <= {@link #getUpperBound()} if applicable.
 * @author renkenh
 */
public interface IIntervalValidator<TYPE extends Comparable<TYPE>> extends IPropertyValidator
{
	
	@Override
	public default ValidatorType getValidatorType()
	{
		return ValidatorType.INTERVAL;
	}


	/**
	 * @return The lower bound if available, otherwise <code>null</code>
	 */
	public TYPE getLowerBound();
	
	/**
	 * @return The upper bound if available, otherwise <code>null</code>
	 */
	public TYPE getUpperBound();

}
