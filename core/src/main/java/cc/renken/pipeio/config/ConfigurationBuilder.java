/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author renkenh
 *
 */
public final class ConfigurationBuilder
{

	private final Map<String, String> values = new HashMap<>();
	private final PropertyModel model;
	

	/**
	 * 
	 */
	private ConfigurationBuilder(PropertyModel model)
	{
		this.model = model;
	}


	public boolean hasPropertyModel()
	{
		return this.getPropertyModel() != null;
	}
	
	public PropertyModel getPropertyModel()
	{
		return this.model;
	}
	
	
	public final ConfigurationBuilder setValue(String key, String value)
	{
		PropertyModel.validateValue(key, value, this.model);
		this.values.put(key, value);
		return this;
	}

	public final ConfigurationBuilder setValue(String key, int value)
	{
		return this.setValue(key, Integer.toString(value));
	}

	public final ConfigurationBuilder setValue(String key, float value)
	{
		return this.setValue(key, Float.toString(value));
	}

	public final ConfigurationBuilder setValue(String key, boolean value)
	{
		return this.setValue(key, Boolean.toString(value));
	}

	public final <E extends Enum<E>> ConfigurationBuilder setValue(String key, E value)
	{
		return this.setValue(key, value.toString());
	}

	
	
	public Configuration compile()
	{
		PropertyModel.validateRequired(this.values, this.model);
		return new Configuration(this.values, this.model);
	}

	
	public static final ConfigurationBuilder forModel(PropertyModel model)
	{
		return new ConfigurationBuilder(model);
	}
	
	public static final ConfigurationBuilder withoutModel()
	{
		return new ConfigurationBuilder(null);
	}
}
