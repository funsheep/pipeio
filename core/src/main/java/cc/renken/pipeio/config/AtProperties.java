/**
 * 
 */
package cc.renken.pipeio.config;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that allows to annotate types with a property system. The system allows to specify properties with default values, required or not, a validation system and
 * descriptions. Furthermore, properties can be categorized which allows for easy clustering of a lot of properties (e.g. for easier displaying it to users).
 * @author renkenh
 */
@Documented
@Retention(RUNTIME)
@Target(TYPE)
public @interface AtProperties
{
	
	public static final String EMPTY = "";
	

	/**
	 * @return All specified proprties.
	 */
	public Property[] value();
	
	/**
	 * The category can be used to cluster properties. Default is the empty string.
	 * @return The category of all properties returned by {@value}.
	 */
	public String category() default EMPTY;

	/**
	 * Annotation that specifies properties, their default value (if any), description and validation.
	 */
	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.TYPE)
	@Repeatable(AtProperties.class)
	public @interface Property
	{
		

		/** The name of the property. */
		public String name();
		
		/**
		 * The default value of the property. An empty string means, that nothing is specified (i.e. there is no default value).
		 * Default value is the empty string.
		 */
		public String value() default EMPTY;
		
		/** Returns whether this property is required or not. */
		public boolean isRequired() default false;

		/**
		 * Defines the validator and its specification for this property.
		 * There are several validators available. Some of them allow the definition of additional validation specification, like specific numeric intervals, etc.
		 */
		public String validationModel() default EMPTY;
		
		/** A description of this property and its usage. Can be shown to users, e.g. in a help system. */
		public String description() default EMPTY;

	}
}
