/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author renkenh
 *
 */
public abstract class AValidator<TYPE extends Comparable<TYPE>> implements IPropertyValidator, IIntervalValidator<TYPE>, IListValidator<TYPE>
{
	
	public static final String INTERVAL_EXPRESSION = "...";

	private final ValidatorType type;
	private final TYPE lowerBound;
	private final TYPE upperBound;
	private final List<TYPE> validValues;

	
	/**
	 * 
	 */
	public AValidator(String validationSpecs)
	{
		ValidatorType type = null;
		TYPE lower = null;
		TYPE upper = null;
		List<TYPE> valid = null;
		if (validationSpecs.contains(INTERVAL_EXPRESSION))
		{
			//initialize lower and upper
			type = ValidatorType.INTERVAL;
			String[] bounds = validationSpecs.split(INTERVAL_EXPRESSION);
			lower = this.convertSpec(bounds[0]);
			upper = this.convertSpec(bounds[1]);
		}
		else if (validationSpecs.contains(","))
		{
			type = ValidatorType.LIST;
			valid = Arrays.stream(validationSpecs.split(",")).map(this::convertSpec).collect(Collectors.toList());
		}
		this.type = type;
		this.lowerBound = lower;
		this.upperBound = upper;
		this.validValues = valid != null ? Collections.unmodifiableList(valid) : null;
	}
	
	public AValidator(TYPE lowerBound, TYPE upperBound)
	{
		this.type = ValidatorType.INTERVAL;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.validValues = null;
	}

	public AValidator(Collection<TYPE> validValueList)
	{
		this.type = ValidatorType.LIST;
		this.lowerBound = null;
		this.upperBound = null;
		this.validValues = Collections.unmodifiableList(new ArrayList<>(validValueList));
	}
	

	protected abstract TYPE convertSpec(String word);


	@Override
	public String validate(String value2Validate)
	{
		switch (this.getValidatorType())
		{
			case INTERVAL:
				TYPE value = this.convertSpec(value2Validate);
				if (value.compareTo(this.lowerBound) >= 0 && value.compareTo(this.upperBound) < 0)
					return null;
				return "Given value is not within the interval of " + this.lowerBound + " and " + this.upperBound;
			case LIST:
				value = this.convertSpec(value2Validate);
				if (this.validValues.contains(value))
					return null;
				return IListValidator.ERROR_TEXT;
			default:
				throw new IllegalStateException("Undefined type. Cannot validate.");
		}
	}


	@Override
	public List<TYPE> getValidValues()
	{
		if (this.getValidatorType() != ValidatorType.LIST)
			throw new IllegalStateException("Not as list validator initialized");
		return this.validValues;
	}

	@Override
	public TYPE getLowerBound()
	{
		if (this.getValidatorType() != ValidatorType.INTERVAL)
			throw new IllegalStateException("Not as interval validator initialized");
		return this.lowerBound;
	}

	@Override
	public TYPE getUpperBound()
	{
		if (this.getValidatorType() != ValidatorType.INTERVAL)
			throw new IllegalStateException("Not as interval validator initialized");
		return this.upperBound;
	}

	@Override
	public ValidatorType getValidatorType()
	{
		return this.type;
	}

}
