/**
 * 
 */
package cc.renken.pipeio.config;

import cc.renken.pipeio.config.IPropertyValidator.AtValidatorConfigs.Validator;

/**
 * @author renkenh
 *
 */
@Validator("boolean")
public class BooleanValidator implements IPropertyValidator
{

	/**
	 * @param validationSpecs
	 */
	public BooleanValidator(String validationSpecs)
	{
		//ingnore specs
	}


	@Override
	public ValueType getValueType()
	{
		return ValueType.BOOLEAN;
	}

	@Override
	public String validate(String value2Validate)
	{
		if (value2Validate.equalsIgnoreCase("true") || value2Validate.equalsIgnoreCase("false"))
			return null;
		return "Given value is not a boolean.";
	}

}
