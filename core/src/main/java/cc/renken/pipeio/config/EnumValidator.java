/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import cc.renken.pipeio.config.IPropertyValidator.AtValidatorConfigs.Validator;
import cc.renken.pipeio.util.EnumUtils;

/**
 * @author renkenh
 *
 */
@Validator("enum")
public class EnumValidator implements IListValidator<String>
{

	private final String validationSpecs;
	private final List<String> validEnumValues;
	
	/**
	 * @param validationSpecs
	 */
	public EnumValidator(String validationSpecs)
	{
		this.validationSpecs = validationSpecs;
		this.validEnumValues = Collections.unmodifiableList(EnumUtils.getEnumValues(validationSpecs).stream().map(Object::toString).collect(Collectors.toList()));
	}

	
	public String enumClass()
	{
		return this.validationSpecs;
	}

	@Override
	public ValueType getValueType()
	{
		return ValueType.STRING;
	}


	@Override
	public String validate(String value2Validate)
	{
		if (this.getValidValues().contains(value2Validate))
			return null;
		return IListValidator.ERROR_TEXT;
	}


	@Override
	public List<String> getValidValues()
	{
		return this.validEnumValues;
	}


}
