/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * @author renkenh
 *
 */
public final class PropertyModelBuilder
{	

	private final Map<String, AtProperties.Property> properties = new HashMap<>();
	private final Map<String, Collection<String>> propertyNamesByCategory = new HashMap<>();

	
	public PropertyModelBuilder addPropertiesFromType(Class<?> clazz)
	{
		AtProperties properties = clazz.getAnnotation(AtProperties.class);
		if (properties != null)
			this.addProperties(properties);
		return this;
	}
	
	public PropertyModelBuilder addProperties(AtProperties properties)
	{
		for (AtProperties.Property p : properties.value())
			this.addProperty(p, properties.category());
		return this;
	}
	
	public PropertyModelBuilder addProperty(AtProperties.Property property)
	{
		return this.addProperty(property, AtProperties.EMPTY);
	}
	
	public PropertyModelBuilder addProperty(AtProperties.Property property, String category)
	{
		if (this.properties.containsKey(property.name()))
			throw new IllegalArgumentException("A property with name "+property.name()+" is known already.");
		this.properties.put(property.name(), property);
		this.addToCategory(property, category);
		return this;
	}
	
	private void addToCategory(AtProperties.Property property, String category)
	{
		Collection<String> properties = this.propertyNamesByCategory.get(category);
		if (properties == null)
		{
			properties = new HashSet<>();
			this.propertyNamesByCategory.put(category, properties);
		}
		properties.add(property.name());
	}

	
	public PropertyModel compile()
	{
		return new PropertyModel(this.properties.values(), this.propertyNamesByCategory);
	}

	public static final PropertyModelBuilder create()
	{
		return new PropertyModelBuilder();
	}
}
