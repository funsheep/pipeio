/**
 * 
 */
package cc.renken.pipeio.config;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import cc.renken.pipeio.config.IPropertyValidator.ValidatorType;
import cc.renken.pipeio.config.IPropertyValidator.ValueType;

/**
 * @author renkenh
 *
 */
public final class PropertyModel
{
	
	public static class Property
	{
		
		private final AtProperties.Property internal;
		private IPropertyValidator validator;
		
		private Property(AtProperties.Property internal)
		{
			this.internal = internal;
		}
		

		/** The name of the property. */
		public String name()
		{
			return this.internal.name();
		}
		
		public ValidatorType validatorType()
		{
			return this.getValidator().getValidatorType();
		}
		
		public ValueType valueType()
		{
			return this.getValidator().getValueType();
		}
		
		@SuppressWarnings("unchecked")
		public synchronized <T extends IPropertyValidator> T getValidator()
		{
			if (this.validator == null)
				this.validator = Validators.getValidatorFor(this.internal);
			return (T) this.validator;
		}
		
		/**
		 * The default value of the property. An empty string means, that nothing is specified (i.e. there is no default value).
		 * Default value is the empty string.
		 */
		public String defaultValue()
		{
			String value = this.internal.value();
			if (value.trim().equals(AtProperties.EMPTY))
				return null;
			return value;
		}
		
		public boolean hasDefaultValue()
		{
			return this.defaultValue() != null;
		}
		
		/** Returns whether this property is required or not. */
		public boolean isRequired()
		{
			return this.internal.isRequired();
		}
		
		/** A description of this property and its usage. Can be shown to users, e.g. in a help system. */
		public String description()
		{
			return this.internal.description();
		}

	}
	

	private final Map<String, Property> properties;
	private final Map<String, Collection<Property>> propertiesByCategory;
	
	
	/**
	 * 
	 */
	PropertyModel(Collection<AtProperties.Property> properties, Map<String, Collection<String>> propertyNamesByCategory)
	{
		this.properties = new HashMap<>();
		properties.stream().map(Property::new).forEach((p) -> this.properties.put(p.name(), p));
		this.propertiesByCategory = new HashMap<>();
		for (Map.Entry<String, Collection<String>> category : propertyNamesByCategory.entrySet())
		{
			if (!category.getValue().isEmpty())
				this.propertiesByCategory.put(category.getKey(), category.getValue().stream().map((s) -> this.properties.get(s)).collect(Collectors.toSet()));
		}
	}
	
	
	public Property getProperty(String name)
	{
		return this.properties.get(name);
	}
	
	public Collection<Property> getProperties()
	{
		return Collections.unmodifiableCollection(this.properties.values());
	}
	
	public Collection<Property> getPropertiesWithoutCategory()
	{
		return this.getProperties(AtProperties.EMPTY);
	}

	public Collection<Property> getProperties(String category)
	{
		Collection<Property> prop = this.propertiesByCategory.get(category);
		if (prop == null)
			return Collections.emptySet();
		
		return Collections.unmodifiableCollection(prop);
	}
	
	public static final void validate(Configuration config, PropertyModel model)
	{
		for (Entry<String, String> entry : config.getMap().entrySet())
			validateValue(entry.getKey(), entry.getValue(), model);
		validateRequired(config.getMap(), model);
	}

	protected static final void validateValue(String key, String value, PropertyModel model)
	{
		if (model == null)
			return;
		Property p = model.getProperty(key);
		if (p == null)
			return;
		IPropertyValidator v = p.getValidator();
		if (v == null)
			return;
		String error = v.validate(value);
		if (error == null)
			return;
		throw new IllegalArgumentException(error);
	}
	
	protected static final void validateRequired(Map<String, String> values, PropertyModel model)
	{
		if (model == null)
			return;
		Property missing = model.getProperties().stream().filter(Property::isRequired).filter((p) -> !p.hasDefaultValue()).filter((p) -> !values.containsKey(p.name())).findAny().orElse(null);
		if (missing == null)
			return;
		throw new IllegalStateException("Required property "+missing.name()+" neither has a default value nor a specified one.");
	}
	

}
