/**
 * 
 */
package cc.renken.pipeio.config;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import cc.renken.pipeio.config.AtProperties.Property;
import cc.renken.pipeio.config.IPropertyValidator.AtValidatorConfigs.Validator;

/**
 * Hub for the validation framework.
 * Allows registration of custom validators and instantiation of them with the specific definition string.
 * @author renkenh
 *
 */
public final class Validators
{
	
	private static final Map<String, Class<? extends IPropertyValidator>> VALIDATORS = new HashMap<>();
	

	static
	{
		registerValidator(BooleanValidator.class);
		registerValidator(IntegerValidator.class);
		registerValidator(FloatValidator.class);
		registerValidator(StringValidator.class);
		registerValidator(EnumValidator.class);
	}

	
	public static final void registerValidator(Class<? extends IPropertyValidator> validatorClass)
	{
		Validator[] configs = validatorClass.getAnnotationsByType(Validator.class);
		for (Validator c : configs)
			registerValidator(c.value(), validatorClass);
	}

	public static final void registerValidator(String identifier, Class<? extends IPropertyValidator> validatorClass)
	{
		if (validatorClass == null)
			throw new IllegalArgumentException();
		if (VALIDATORS.containsKey(identifier))
			throw new IllegalArgumentException("For given identifier " + identifier + " a validator class is registered already.");
		VALIDATORS.put(identifier, validatorClass);
	}

	
	public static final IPropertyValidator getValidatorFor(Property property)
	{
		return getValidatorFor(property.validationModel());
	}
	
	public static final IPropertyValidator getValidatorFor(String validationSpec)
	{
		if (validationSpec.trim().isEmpty())
			return null;
		int index = validationSpec.indexOf(':');
		if (index == 0)
			return null;

		final String valueType = index > 0 ? validationSpec.substring(0, index) : validationSpec;
		final String validateConfig = index == -1 || index >= validationSpec.length()-1 ? null : validationSpec.substring(index);
		
		if (valueType.equals("class"))
			return instantiateValidator(validateConfig);

		Class<? extends IPropertyValidator> clazz = VALIDATORS.get(valueType);
		if (clazz == null)
			throw new RuntimeException("Could not find appropriate validator for " + valueType);
		return instantiateValidator(clazz, validateConfig);
	}
	

	@SuppressWarnings("unchecked")
	private static final IPropertyValidator instantiateValidator(String validatorSpec)
	{
		try
		{
			String[] split = validatorSpec.split(":");
			Class<?> clazz = Class.forName(split[0]);
			if (!IPropertyValidator.class.isAssignableFrom(clazz))
				throw new RuntimeException("Given class "+split[0]+" is not of type IPropertyValidator.");
			return instantiateValidator((Class<? extends IPropertyValidator>) clazz, split.length > 1 && split[1].trim().length() > 0 ? split[1] : null);
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	private static final IPropertyValidator instantiateValidator(Class<? extends IPropertyValidator> clazz, String validatorSpec)
	{
		try
		{
			if (validatorSpec == null)
				return (IPropertyValidator) clazz.getConstructor().newInstance();
			return (IPropertyValidator) clazz.getConstructor(String.class).newInstance(validatorSpec);
		}
		catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e)
		{
			throw new RuntimeException(e);
		}
	}

	private Validators()
	{
		//no instance
	}

}
