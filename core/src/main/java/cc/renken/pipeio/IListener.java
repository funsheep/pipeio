/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

/**
 * Listener that can be registered at the pipeline to be notified about state changes, communication and exceptions.
 * These listeners should not be used to implement communication or state handling logic. Instead, use the {@link IFaucet}
 * for this. The {@link IFaucet} is always called first and especially from within the pipeline thread. This allows the {@link IFaucet}
 * implementation to directly react to events (and especially to every event).
 *
 * The pipeline allows the registration of listeners. Listeners and all methods called on them are handled by a separate thread - and not by the
 * pipeline thread.
 *
 * @author Hendrik Renken
 */
@FunctionalInterface
public interface IListener
{

	/** The types of events that this listener may receive. */
	public enum EventType
	{
		/** The state {@link IPipeline#getState()} has changed. The new state is send as payload along. */
		STATE_CHANGED,
		/** Every received data entity that is also pushed into the {@link IFaucet} is send to the listener. The entity is
		 * send as payload. Note: This does especially not include any raw entities transformed within the pipeline. */
		DATA_RECEIVED,
		/** Every data that is send through the faucet into the pipeline is also being send to every registered
		 * listener. The send data entity is included as payload. */
		DATA_SEND,
		/** Every unexpected exception that occurs is send to every registered listener. The exception itself is send along as payload. */
		EXCEPTION_OCCURRED
	}


	/**
	 * The handle event method. See the {@link EventType}s for information over the type of the payload.
	 * Please do not use the listener for code that manages the pipeline. Use the {@link IFaucet} implementation for this.
	 * @param type The type of the event.
	 * @param payload The payload. See the {@link EventType}s javadoc for more information.
	 */
	public void handleEvent(EventType type, Object payload);

}
