package cc.renken.pipeio;

/** The different states the pipe and components can be in. */
public enum State
{
	
	/** The pipe has been setup */
	ACTIVE,
	/** The send packages will not be processed by the sink. At least one component of the pipe does not work and is not {@link IComponent#isActive()}. */ 
	INACTIVE,
	/** The pipe has been shutdown and disconnected. Any functionality other then listener management and getting a state are not available. The pipe can be reactivated by calling {@link IPipeline#activate()} */
	DEACTIVATED
}