/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.io.IOException;

import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.config.PropertyModel;

/**
 * The pipe. The main interface. Can be used for async or sync communication.
 * 
 * The lifecycle is as follows: The pipeline can be in three different states. After creation, the pipeline resides in state
 * {@value State#DEACTIVATED}. In this state it does nothing and allows nothing to be send or received.
 * 
 * After a call to {@link #activate()} the pipeline tries to activate itself. That means, every component within the pipeline (starting at the sink)
 * is tried to be activated. If one of the components fails and just does not activate, the pipeline will be in state {@value State#INACTIVE}.
 * 
 * However, if the activation was successful and every component is {@link IComponent#getState()} (== <code>true</code>), then the pipeline is in
 * state {@value State#ACTIVE}.
 * 
 * If some kind of error occurs and any component switches from active to inactive {@link IComponent#getState()} (== <code>false</code>), the pipeline
 * switches to {@value State#INACTIVE} (and vice versa when all errors have been resolved again).
 * 
 * Within the documentation data flow directions are often described with "up" or "upward" and "down" or "downward". "Up" means that data flows from the {@link ISink} (i.e. the communication partner)
 * to the {@link IFaucet}. "Down" means, that data flows from the application logic through the {@link IFaucet} into the {@link ISink} (i.e. to the communication partner).
 *
 * The pipeline allows the registration of listeners. Listeners and all methods called on them are handled by a separate thread - and not by the
 * pipeline thread.
 * @author Hendrik Renken
 */
public interface IPipeline<RECV, PUSH>
{

	/**
	 * An id that should appropriately identify this pipeline. This id is also used in any threadnames to be able to
	 * quickly identify threads used by this pipeline. Therefore, this pipeline could, e.g. contain hints regarding its communication partner
	 * (like an IP-address or a port name).
	 * @return An id for uniquely and appropriately identifying this pipeline.
	 */
	public String id();

	/**
	 * Activates the pipeline. The pipeline now activates every component starting from the {@link ISink}. That means, the pipeline calls
	 * {@link IComponent#activate()}. The pipeline does not care whether the activation is successful or not. Based on the outcome the pipeline
	 * resides either in {@link State#ACTIVE} or {@link State#INACTIVE}. If the pipeline is already in one of these states a call to this method does
	 * nothing. To "reactivate" a pipeline - first a call to deactivate has to be made.
	 * 
	 * This method throws an exception if something goes wrong during the activation of the components. If a component throws an exception
	 * the activation process stops. Already active components are left activated. Furthermore, the listeners are informed about the exception.
	 * The pipeline is left in {@link State#DEACTIVATED}.
	 * 
	 * @throws IOException If something goes wrong during activation of the components. The original exception is included in the IOException as the cause.
	 */
	public void activate(Configuration config) throws IOException;

	/**
	 * @return The current state of the pipe.
	 * @see State for more information about the states.
	 * 
	 * @throws IOException If the state could not be determined because a timeout or a component threw an exception on accessing its state.
	 */
	public State getState() throws IOException;

	/**
	 * Deactivates the pipeline. That means, on each component starting from the {@link IFaucet} {@link IComponent#deactivate()} is called.
	 * If the pipeline is in state {@link State#DEACTIVATED} a call to this method does nothing.
	 * 
	 * An exception is thrown when a component throws an exception on deactivation. Then the deactivation process stops. Still active
	 * components are left active. For complete deactivation even when an error occurs use {@link #shutdown()}. 
	 * 
	 * @throws IOException when a component throws an exception on deactivation.
	 */
	public void deactivate() throws IOException;

	//FIXME add javadoc.
//	public void shutdown();

	public PropertyModel getConfigurationModel();
	
	/**
	 * Registers a listener for events from this pipeline. If the listener has been registered before, nothing happens.
	 * @param listener The listener to register.
	 */
	public void addListener(IListener listener);

	/**
	 * Removes the given listener. If the listener was not registered before, nothing happens.
	 * @param listener The listener to remove.
	 */
	public void removeListener(IListener listener);

}
