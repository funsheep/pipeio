/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IComponentContainer;
import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.IScheduler;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public abstract class AComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT, C extends IComponent<RECV_OUT, PUSH_OUT>> implements IComponentContainer<RECV_OUT, PUSH_OUT>
{
	
//	private static final Logger logger = LoggerFactory.getLogger(AComponentHandler.class);
	
	protected final C component; 
	private AComponentContainer<RECV_OUT, ?, ?, PUSH_IN, ?> previous;
	private AComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next;
	private final IExceptionHandler exceptionHandler;
	private final Scheduler scheduler;
	private State lastState = State.DEACTIVATED;
	private boolean handlerIsActive = false;
	private Configuration currentConfig;


	/**
	 * 
	 */
	public AComponentContainer(Scheduler scheduler, C component, AComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next, IExceptionHandler exceptionHandler)
	{
		this.scheduler = scheduler;
		this.component = component;
		this.exceptionHandler = exceptionHandler;
		if (next != null)
		{
			this.next = next;
			assert next.previous == null;
			next.previous = this;
		}
	}


	@Override
	public void notifyActiveStateChanged()
	{
		if (!this.isHandlerActive())
			throw new IllegalStateException("Container is deactivated.");
		if (this.updateLastState())
		{
			this.component.activeStateChanged();
			this.previous.notifyActiveStateChanged();
		}
	}
	
	@Override
	public State getNextState()
	{
		assert this.assertIsMe();
		if (!this.hasNext())
			return null;
		return this.next().getState();
	}

	@Override
	public IScheduler getScheduler()
	{
		return this.scheduler;
	}

	@Override
	public void exceptionEncountered(Exception ex)
	{
		this.exceptionHandler.exceptionEncounteredNotifyConsumers(ex);
	}
	
	protected AComponentContainer<RECV_OUT, ?, ?, PUSH_IN, ?> previous()
	{
		return this.previous;
	}

	protected AComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next()
	{
		return this.next;
	}

	protected final boolean isHandlerActive()
	{
		return this.handlerIsActive;
	}

	protected final State getState()
	{
		assert this.assertIsMe();
		if (!this.isHandlerActive())
			return State.DEACTIVATED;
		if (this.component.getState() == State.DEACTIVATED)
			return State.DEACTIVATED;
		if (this.hasNext() && this.next().getState() == State.DEACTIVATED)
			return State.DEACTIVATED;

		if (this.component.getState() == State.INACTIVE)
			return State.INACTIVE;
		if (this.hasNext() && this.next().getState() == State.INACTIVE)
			return State.INACTIVE;

		assert this.isHandlerActive() && this.component.getState() == State.ACTIVE && (!this.hasNext() || this.next().getState() == State.ACTIVE);
		return State.ACTIVE;
	}

	protected final boolean hasNext()
	{
		assert this.assertIsMe();
		return this.next != null;
	}

	protected final boolean hasPrevious()
	{
		assert this.assertIsMe();
		return this.previous != null;
	}

	@Override
	public void activateNext() throws IOException, TimeoutException
	{
		assert this.assertIsMe();
		this.next().activate(this.currentConfig);
	}
	
	protected final void activate(Configuration config) throws IOException, TimeoutException
	{
		assert this.assertIsMe();
		if (this.isHandlerActive())
			return;
		if (this.hasNext())
			this.next.activate(config);

		this.handlerIsActive = true;
		this.currentConfig = config;
		this.component.activate(config);
		this.updateLastState();
	}
	
	
	protected abstract void pushIn(PUSH_IN entity) throws IOException, TimeoutException;
	
	protected abstract void receiveIn(RECV_IN entity);
	
	protected final boolean updateLastState()
	{
		assert this.assertIsMe();
		State state = this.getState();
		if (this.lastState == state)
			return false;
			
		this.lastState = state;
		return true;
	}


	protected final void deactivate()
	{
		assert this.assertIsMe();
		this.component.deactivate();

		this.handlerIsActive = false;
		this.updateLastState();
		if (this.hasNext())
			this.next.deactivate();
	}
	
	public void deactivateNext()
	{
		assert this.assertIsMe();
		this.next().deactivate();
	}

	protected boolean assertIsMe()
	{
		return this.getScheduler().isMe();
	}

}
