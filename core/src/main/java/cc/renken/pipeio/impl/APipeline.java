/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.IPipeline;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.config.PropertyModel;
import cc.renken.pipeio.IListener.EventType;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
abstract class APipeline<RECV, PUSH> implements IPipeline<RECV, PUSH>
{
	
	private static final Logger logger = LoggerFactory.getLogger(APipeline.class);

	private final String id;
	private final EventNotifier notifier;
	private final Scheduler pipeScheduler = new Scheduler("Pipelinescheduler", this.id(), this::exceptionEncounteredNotifyListeners);

	private boolean isDeactivated = true;
	

	/**
	 * Constructor. Creates a new pipeline without an Id, i.e. {@link #id()} will return <code>null</code>.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	public APipeline()
	{
		this(null);
	}
	
	/**
	 * Constructor. Creates a new pipeline with an id. The id is used throughout the pipeline to identify threads, components and errors.
	 * @param id The id of the pipeline. May be null.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	public APipeline(String id)
	{
		this.id = id;
		this.notifier = new EventNotifier(id, this::exceptionEncounteredNotifyListeners);
	}

	
	protected abstract void activateFaucet(Configuration config) throws IOException, TimeoutException;
	
	protected abstract State getFaucetState();
	
	protected abstract void deactivateFaucet();


	@Override
	public String id()
	{
		return this.id;
	}

	@Override
	public void activate(Configuration config) throws IOException
	{
		PropertyModel.validate(config, this.getConfigurationModel());
		try
		{
			this.scheduler().activate();
			this.scheduler().waitForExec(() ->
			{
				if (this.state() != State.DEACTIVATED)
					return;
				
				this.activateFaucet(config);
				this.setDeactivated(false);
			});
		}
		catch (ExecutionException | TimeoutException e)
		{	
			//extract real cause from execution
			Exception ex = e;
			if (ex instanceof ExecutionException)
				ex = (Exception) ((ExecutionException) ex).getCause();
			this.exceptionEncounteredNotifyListeners(ex);
			logger.error("Could not activate pipeline {}", this.id() != null ? this.id() : "", ex);
			throw new IOException(ex);
		}
	}
	

	@Override
	public State getState() throws IOException
	{
		try
		{
			return this.state();
		}
		catch (ExecutionException | TimeoutException e)
		{
			//extract real cause from execution
			Exception ex = e;
			if (ex instanceof ExecutionException)
				ex = (Exception) ((ExecutionException) ex).getCause();
			this.exceptionEncounteredNotifyListeners(ex);
			logger.error("Could not get state from pipeline {}", this.id() != null ? this.id() : "", ex);
			throw new IOException(ex);
		}
	}

	private State state() throws ExecutionException, TimeoutException
	{
		Callable<State> call = () ->
		{
			assert !this.isDeactivated || this.isDeactivated && this.getFaucetState() == State.DEACTIVATED;
			if (this.isDeactivated)
				return State.DEACTIVATED;
			return this.getFaucetState();
		};
		if (!this.scheduler().isActive())
			try
			{
				return call.call();
			}
			catch (Exception e)
			{
				this.exceptionEncounteredNotifyListeners(e);
				throw new ExecutionException(e);
			}
		return this.scheduler().waitForExec(call);
	}

	@Override
	public void deactivate() throws IOException
	{
		try
		{
			this.scheduler().waitForExec(() ->
			{
				if (this.state() == State.DEACTIVATED)
					return;
				
				this.deactivateFaucet();
				this.setDeactivated(true);
			});
		}
		catch (ExecutionException | TimeoutException e)
		{
			//extract real cause from execution
			Exception ex = e;
			if (ex instanceof ExecutionException)
				ex = (Exception) ((ExecutionException) ex).getCause();
			this.exceptionEncounteredNotifyListeners(ex);
			logger.error("Could not get state from pipeline {}", this.id() != null ? this.id() : "", ex);
			throw new IOException(ex);
		}
	}
	
	private void setDeactivated(boolean isDeactivated) throws ExecutionException, TimeoutException
	{
		this.scheduler().waitForExec(() ->
		{
			this.isDeactivated = isDeactivated;
			this.stateChangedNotifyListeners();
			if (this.isDeactivated)
				this.scheduler().deactivate();
			else
				this.scheduler().activate();
		});
	}

	private State lastState = State.DEACTIVATED;
	/**
	 * Notifies listeners about a state change. The method caches the last send state and sends out notifications on real changes
	 * only. Notifications about "fake changes" that occur due to events that do not actually change the state are omitted.
	 * @throws ExecutionException If the state could not be retrieved because of a failing component.
	 * @throws TimeoutException If the state could not be retrieved in time.
	 * @see #getState()
	 */
	protected void stateChangedNotifyListeners() throws ExecutionException, TimeoutException
	{
		assert this.scheduler().isMe();
		State current = this.state();
		if (this.lastState == current)
			return;
		this.lastState = current;
		this.notifier.handleEvent(EventType.STATE_CHANGED, current);
	}
	
	/**
	 * Notifies listener about retrieved entities.
	 * @param entity The entity that was retrieved by the pipeline.
	 */
	protected void receivedNotifyListeners(RECV entity)
	{
		this.notifier.handleEvent(EventType.DATA_RECEIVED, entity);
	}

	/**
	 * Notifies listener about a successfully send entity. Please note, that listeners should not be informed through this method
	 * about failed sending. I.e. call this method after sending the data - not before.
	 * @param entity The entity that was send.
	 */
	protected void sendNotifyListeners(PUSH entity)
	{
		this.notifier.handleEvent(EventType.DATA_SEND, entity);
	}

	/**
	 * Notify listeners that an unexpected exception has occurred. 
	 * @param ex The exception that has occurred.
	 */
	protected void exceptionEncounteredNotifyListeners(Exception ex)
	{
		this.notifier.handleEvent(EventType.EXCEPTION_OCCURRED, ex);
	}

	/**
	 * Returns the scheduler that should be used by this pipeline.
	 * @return The scheduler to be used.
	 */
	protected Scheduler scheduler()
	{
		return this.pipeScheduler;
	}

	@Override
	public void addListener(IListener consumer)
	{
		this.notifier.addListener(consumer);
	}

	@Override
	public void removeListener(IListener consumer)
	{
		this.notifier.removeListener(consumer);
	}

}
