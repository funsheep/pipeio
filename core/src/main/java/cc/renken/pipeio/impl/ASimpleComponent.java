/**
 * 
 */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
public abstract class ASimpleComponent<RECV_OUT, PUSH_OUT> implements IComponent<RECV_OUT, PUSH_OUT>
{
	
	private boolean isActive = false;

	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}
	
	@Override
	public void deactivate()
	{
		this.isActive = false;
	}

}
