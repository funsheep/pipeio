/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.IScheduler;
import cc.renken.pipeio.util.Executor;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class Scheduler implements IScheduler
{
	
	private final Logger logger;

	
	private final class SaveRunnable implements Runnable
	{
		private final FailingRunnable run;
		
		public SaveRunnable(FailingRunnable run)
		{
			this.run = run;
		}
		
		@Override
		public void run()
		{
			if (!Scheduler.this.isActive())
			{
				logger.debug("Tried to execute runnable on deactivation.");
				return;
			}
			try
			{
				this.run.run();
			}
			catch (Exception e)
			{
				logger.error("Exception occurred while executing task.", e);
				Scheduler.this.exceptionHandler.exceptionEncounteredNotifyConsumers(e);
			}
		}
	}
	
	

	private static final String DEFAULT_THREADGROUP = "cc.renken.pipeio";
	private static final String DEFAULT_NAME = "Unknown";

	
	private final IExceptionHandler exceptionHandler;
	private final String name;
	private final String threadGroup;
	private final ReentrantLock lock = new ReentrantLock();

	private Executor executor;
	

	public Scheduler(String name, String threadGroup, IExceptionHandler exceptionHandler)
	{
		this.logger = LoggerFactory.getLogger(String.valueOf(threadGroup) + "-" + String.valueOf(name));
		this.exceptionHandler = exceptionHandler;
		this.threadGroup = threadGroup != null ? threadGroup : DEFAULT_THREADGROUP;
		this.name = name != null ? name : DEFAULT_NAME;
	}

	
	public void activate()
	{
		this.lock.lock();
		try
		{
			if (this.isActive())
				return;
			this.executor = new Executor(this.name, this.threadGroup, this.exceptionHandler);
		}
		finally
		{
			this.lock.unlock();
		}
	}
	
	public boolean isActive()
	{
		this.lock.lock();
		try
		{
			return this.executor != null;
		}
		finally
		{
			this.lock.unlock();
		}
	}
	
	public void deactivate()
	{
		this.lock.lock();
		try
		{
			if (!this.isActive())
				return;
			this.executor.shutdown();
			this.executor = null;
		}
		finally
		{
			this.lock.unlock();
		}
	}
	
	
	@Override
	public boolean isMe()
	{
		this.lock.lock();
		try
		{
			return this.executor != null && this.executor.isMe();
		}
		finally
		{
			this.lock.unlock();
		}
	}

	@Override
	public ITask submit(FailingRunnable run) throws RejectedExecutionException
	{
		this.lock.lock();
		try
		{
			if (!this.isActive())
				throw new RejectedExecutionException("Scheduler is inactive.");
			return this.executor.submit(new SaveRunnable(run));
		}
		finally
		{
			this.lock.unlock();
		}
	}

	@Override
	public ITask schedule(FailingRunnable run, int delay, TimeUnit unit) throws RejectedExecutionException
	{
		this.lock.lock();
		try
		{
			if (!this.isActive())
				throw new RejectedExecutionException("Scheduler is inactive.");
			return this.executor.schedule(new SaveRunnable(run), delay, unit);
		}
		finally
		{
			this.lock.unlock();
		}
	}

	@Override
	public ITask schedulePeriodically(FailingRunnable run, int delay, int period, TimeUnit unit) throws RejectedExecutionException
	{
		this.lock.lock();
		try
		{
			if (!this.isActive())
				throw new RejectedExecutionException("Scheduler is inactive.");
			return this.executor.schedulePeriodically(new SaveRunnable(run), delay, period, unit);
		}
		finally
		{
			this.lock.unlock();
		}
	}

	public void waitForExec(FailingRunnable run) throws ExecutionException, TimeoutException
	{
		Executor executor;
		this.lock.lock();
		try
		{
			if (!this.isActive())
				throw new RejectedExecutionException("Scheduler is inactive.");
			executor = this.executor;
		}
		finally
		{
			this.lock.unlock();
		}
		executor.waitForExec(() -> { run.run(); return null; });
	}
	
	public <V> V waitForExec(Callable<V> call) throws ExecutionException, TimeoutException
	{
		Executor executor;
		this.lock.lock();
		try
		{
			if (!this.isActive())
				throw new RejectedExecutionException("Scheduler is inactive.");
			executor = this.executor;
		}
		finally
		{
			this.lock.unlock();
		}
		return executor.waitForExec(call);
	}
	
}
