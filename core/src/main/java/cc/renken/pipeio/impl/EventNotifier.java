/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.IListener;
import cc.renken.pipeio.IListener.EventType;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public final class EventNotifier
{

	private static final Logger logger = LoggerFactory.getLogger(EventNotifier.class);

	private final Scheduler eventScheduler;
	private final LinkedHashSet<IListener> consumers = new LinkedHashSet<>(1);


	public EventNotifier(String name, IExceptionHandler handler)
	{
		this.eventScheduler = new Scheduler("Eventscheduler", name, handler);
		this.eventScheduler.activate();
	}
	
	

	public final void addListener(IListener consumer)
	{
		this.eventScheduler.submit(() -> this.consumers.add(consumer));
	}

	public final List<IListener> getListeners()
	{
		try
		{
			return this.eventScheduler.waitForExec(() -> new ArrayList<>(this.consumers));
		}
		catch (Exception e)
		{
			logger.error("Could not get listeners.", e);
			return Collections.emptyList();
		}
	}

	public final void removeListener(IListener consumer)
	{
		this.eventScheduler.submit(() -> this.consumers.remove(consumer));
	}


	public final void shutdown()
	{
		this.eventScheduler.deactivate();
	}

	public final void handleEvent(EventType type, Object payload)
	{
		logger.debug("Notifying: Event {} with payload {}.", type, payload);
		this.eventScheduler.submit(() ->
		{
			for (IListener listener : this.consumers)
				handleEvent(type, payload, listener);
		});
	}

	private static final void handleEvent(EventType type, Object payload, IListener handler)
	{
		try
		{
			handler.handleEvent(type, payload);
		}
		catch (Exception ex)
		{
			logger.warn("One of the handlers threw an exception while handling event {} with payload {}.", type, payload, ex);
		}
	}

}
