/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public interface IScheduler
{
	
	@FunctionalInterface
	public interface FailingRunnable
	{
		public void run() throws Exception;
	}


	public interface ITask
	{
		public void cancel();
		
		public boolean isCanceled();
		
		public boolean isDone();
	}

	public boolean isMe();
	
	public ITask submit(FailingRunnable run) throws RejectedExecutionException;

	public ITask schedule(FailingRunnable run, int delay, TimeUnit unit) throws RejectedExecutionException;

	public ITask schedulePeriodically(FailingRunnable run, int delay, int period, TimeUnit unit) throws RejectedExecutionException;

}
