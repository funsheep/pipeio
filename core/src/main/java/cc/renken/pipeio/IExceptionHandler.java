package cc.renken.pipeio;

@FunctionalInterface
public interface IExceptionHandler
{
	public void exceptionEncounteredNotifyConsumers(Exception ex);
}