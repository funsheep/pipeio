/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.util;

import java.util.concurrent.ThreadFactory;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
class SelfawareThreadFactory implements ThreadFactory
{
	
	private final String name;
	private final ThreadGroup threadGroup;
	private final boolean daemon;
	private Thread createdThread;
	
	
	public SelfawareThreadFactory(String threadGroup, boolean daemon)
	{
		this(threadGroup, null, daemon);
	}

	public SelfawareThreadFactory(String threadGroup, String name, boolean daemon)
	{
		this.threadGroup = threadGroup != null ? new ThreadGroup(threadGroup) : null;
		this.name = name;
		this.daemon = daemon;
	}



	@Override
	public Thread newThread(Runnable r)
	{
		if (this.createdThread != null)
			throw new RuntimeException("This should not happen. There should always be one only.");
		if (this.name != null)
			this.createdThread = new Thread(this.threadGroup, r, name);
		else
			this.createdThread = new Thread(this.threadGroup, r);
		this.createdThread.setDaemon(this.daemon);
		if (this.createdThread.getPriority() != Thread.NORM_PRIORITY)
			this.createdThread.setPriority(Thread.NORM_PRIORITY);
		return this.createdThread;
	}
	
	public boolean isMe()
	{
		return Thread.currentThread().equals(this.createdThread);
	}

}
