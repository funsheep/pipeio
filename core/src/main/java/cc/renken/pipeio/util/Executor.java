/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.util;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.IScheduler.ITask;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class Executor
{
	
	private final class FutureTask implements ITask
	{
		
		private final Future<?> future;
		
		public FutureTask(Future<?> future)
		{
			this.future = future;
		}

		@Override
		public void cancel()
		{
			this.future.cancel(true);
		}

		@Override
		public boolean isCanceled()
		{
			return this.future.isCancelled() || Executor.this.executor.isShutdown();
		}

		@Override
		public boolean isDone()
		{
			return this.future.isDone();
		}
		
	}
	

	private static final int DEFAULT_TIMEOUT = 10000;

	
	private final SelfawareThreadFactory pipeThreadFactory;
	private final ScheduledExecutorService executor;
	

	public Executor(IExceptionHandler exceptionHandler)
	{
		this(null, null, exceptionHandler);
	}

	public Executor(String name, String threadGroup, IExceptionHandler exceptionHandler)
	{
		this.pipeThreadFactory = new SelfawareThreadFactory(threadGroup, name, true);
		this.executor = Executors.newSingleThreadScheduledExecutor(this.pipeThreadFactory);
	}

	
	public final boolean isMe()
	{
		return this.pipeThreadFactory.isMe();
	}


	public ITask submit(Runnable run) throws RejectedExecutionException
	{
		Future<?> future = this.executor.submit(run);
		return new FutureTask(future);
	}

	public ITask schedule(Runnable run, int delay, TimeUnit unit) throws RejectedExecutionException
	{
		Future<?> future = this.executor.schedule(run, delay, unit);
		return new FutureTask(future);
	}

	public ITask schedulePeriodically(Runnable run, int delay, int period, TimeUnit unit) throws RejectedExecutionException
	{
		Future<?> future = this.executor.scheduleAtFixedRate(run, delay, period, unit);
		return new FutureTask(future);
	}

	public final <V> V waitForExec(Callable<V> call) throws ExecutionException, TimeoutException
	{
		//check thread - if called from executor - exec immediately
		if (this.isMe())
		{
			try
			{
				return call.call();
			}
			catch (Exception e)
			{
				throw new ExecutionException("Task Excecution threw error.", e);
			}
		}
		
		Future<V> task = this.executor.submit(call);
		try
		{
			return task.get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
		}
		catch (InterruptedException ie)
		{
			throw new TimeoutException("Execution was interrupted: " + ie.getMessage());
		}
	}
	
	public void shutdown()
	{
		this.executor.shutdownNow();
	}

}
