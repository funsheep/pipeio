/**
 * 
 */
package cc.renken.pipeio.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author renkenh
 *
 */
public class EnumUtils
{

	public static Collection<Object> getEnumValues(String enumClassName)
	{
		try
		{
			Class<?> enumClass = Class.forName(enumClassName);
			return getEnumValues(enumClass);
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	public static Collection<Object> getEnumValues(Class<?> enumClass)
	{
		try
		{
			Method m = enumClass.getMethod("values");
			Object ret = m.invoke(null);
			Object[] enums = (Object[]) ret;
			return Arrays.asList(enums);
		}
		catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | ClassCastException e)
		{
			throw new RuntimeException(e);
		}
	}


	private EnumUtils()
	{
		//no instance
	}

}
