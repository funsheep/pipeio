/**
 * 
 */
package cc.renken.pipeio.util;

import java.io.Closeable;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author renkenh
 *
 */
public abstract class ARestartableRunner implements Closeable
{
	
	private static final Logger logger = LoggerFactory.getLogger(ARestartableRunner.class);


	private final AtomicBoolean isRunning = new AtomicBoolean(false);
	private final ThreadGroup group;
	private final String name;
	private volatile Thread thread;


	public ARestartableRunner()
	{
		this(null);
	}

	public ARestartableRunner(String name)
	{
		this(name, Thread.currentThread().getThreadGroup());
	}

	/**
	 * 
	 */
	public ARestartableRunner(String name, ThreadGroup group)
	{
		this.name = (name != null) ? name : this.getClass().getSimpleName();
		this.group = group;
	}


	public synchronized void start()
	{
		if (!this.isRunning.get() && (this.thread == null || !this.thread.isInterrupted()))
		{
			this.thread = new Thread(this.group, this::execute, this.name);
			this.thread.setDaemon(true);
			this.isRunning.set(true);
			this.thread.start();
		}
	}
	

	private void execute()
	{
		logger.debug("{} runner starting.", this.getClass().getSimpleName());
		while (this.isRunning())
		{
			try
			{
				this.run();
			}
			catch (InterruptedException ex)
			{
				//do not log this as an error. Just check current running state.
			}
			catch (Exception ex)
			{
				logger.error("{} runner threw exception while executing.", this.getClass().getSimpleName());
				try
				{
					Thread.sleep(4000);
				}
				catch (InterruptedException rupt)
				{
					//do nothing - check if still running.
				}
			}
		}
		logger.debug("{} runner shutting down.", this.getClass().getSimpleName());
		synchronized (this)
		{
			this.thread = null;
		}
	}
	
	protected abstract void run() throws InterruptedException;
	
	public boolean isRunning()
	{
		Thread thread = this.thread;
		return this.isRunning.get() && thread != null && !thread.isInterrupted();
	}
	
	@Override
	public synchronized void close()
	{
		this.isRunning.set(false);
		this.thread.interrupt();
	}

}
