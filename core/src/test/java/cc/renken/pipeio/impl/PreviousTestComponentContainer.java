/**
 * 
 */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.impl.AComponentContainer;
import cc.renken.pipeio.impl.Scheduler;

/**
 * @author renkenh
 *
 */
public class PreviousTestComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> extends TestComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT>
{

	/**
	 * @param scheduler
	 * @param component
	 * @param next
	 * @param exceptionHandler
	 */
	public PreviousTestComponentContainer(Scheduler scheduler, TestComponent<RECV_OUT, PUSH_OUT> component, AComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next, IExceptionHandler exceptionHandler)
	{
		super(scheduler, component, next, exceptionHandler);
	}

	@Override
	public void notifyActiveStateChanged()
	{
		//do nothing
	}

}
