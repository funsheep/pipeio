/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public abstract class ATestComponent<RECV_OUT, PUSH_OUT> implements IComponent<RECV_OUT, PUSH_OUT>
{

	private boolean isActive = false;
	private boolean isShutdown = true;
	private boolean notifiedActiveStateChanged = false;
	

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activate(Configuration config)
	{
		this.isShutdown = false;
		this.isActive = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activeStateChanged()
	{
		this.notifiedActiveStateChanged = true;
	}
	
	public boolean gotActiveStateChanged()
	{
		return this.notifiedActiveStateChanged;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State getState()
	{
		if (this.isShutdown)
			return State.DEACTIVATED;
		if (this.isActive)
			return State.ACTIVE;
		return State.INACTIVE;
	}
	
	public void setActive(boolean active)
	{
		this.isActive = active;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deactivate()
	{
		this.isShutdown = true;
	}

//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	public void shutdown()
//	{
//		this.isShutdown = true;
//	}

}
