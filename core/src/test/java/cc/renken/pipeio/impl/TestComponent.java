/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IComponentContainer;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class TestComponent<RECV_OUT, PUSH_OUT> extends ATestComponent<RECV_OUT, PUSH_OUT>
{

	private IComponentContainer<RECV_OUT, PUSH_OUT> container;

	
	protected void setup(IComponentContainer<RECV_OUT, PUSH_OUT> container)
	{
		this.container = container;
	}


	public void setActive(boolean active)
	{
		this.setActive(active, true);
	}

	public void setActive(boolean active, boolean notify)
	{
		super.setActive(active);
		if (notify)
			this.container.notifyActiveStateChanged();
	}

}
