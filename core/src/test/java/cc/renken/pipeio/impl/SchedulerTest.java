/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.IScheduler.FailingRunnable;
import cc.renken.pipeio.IScheduler.ITask;
import cc.renken.pipeio.impl.Scheduler;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class SchedulerTest
{
	private static final IExceptionHandler HANDLER = (e) -> { fail(); };

	@Test
	public void testScheduler()
	{
		new Scheduler("Test", "Test", HANDLER);
		new Scheduler(null, "Test", HANDLER);
		new Scheduler("Test", null, HANDLER);
		new Scheduler(null, null, HANDLER);
	}

	@Test
	public void testActivate()
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER);
		scheduler.activate();
	}

	@Test
	public void testIsActive()
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER);
		assertFalse(scheduler.isActive());
		scheduler.activate();
		assertTrue(scheduler.isActive());
	}

	@Test
	public void testDeactivate()
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER)
		{
			@Override
			public boolean isMe()
			{
				return true;
			}
		};
		assertFalse(scheduler.isActive());
		scheduler.activate();
		assertTrue(scheduler.isActive());
		scheduler.deactivate();
		assertFalse(scheduler.isActive());
	}

	@Test
	public void testDeActivate()
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER)
		{
			@Override
			public boolean isMe()
			{
				return true;
			}
		};
		assertFalse(scheduler.isActive());
		scheduler.activate();
		assertTrue(scheduler.isActive());
		scheduler.deactivate();
		assertFalse(scheduler.isActive());
		scheduler.activate();
		assertTrue(scheduler.isActive());
		scheduler.deactivate();
		assertFalse(scheduler.isActive());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.Scheduler#isMe()}.
	 */
	@Test
	public void testIsMe() throws Exception
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER);
		assertFalse(scheduler.isMe());
		scheduler.activate();
		Callable<Boolean> c = () -> { return scheduler.isMe(); };
		Boolean b = scheduler.waitForExec(c);
		assertTrue(b.booleanValue());
	}

	@Test
	public void testSubmit() throws Exception
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER);
		assertFalse(scheduler.isMe());
		scheduler.activate();
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		FailingRunnable r = () -> { future.complete(true); };
		scheduler.submit(r);
		assertTrue(future.get(1000, TimeUnit.MILLISECONDS));
	}

	@Test
	public void testSchedule() throws Exception
	{
		int delay = 1000;
		Scheduler scheduler = new Scheduler(null, null, HANDLER);
		assertFalse(scheduler.isMe());
		scheduler.activate();
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		FailingRunnable r = () -> { future.complete(true); };
		scheduler.schedule(r, delay, TimeUnit.MILLISECONDS);
		assertTrue(future.get(1000 + delay, TimeUnit.MILLISECONDS));
	}

	@Test
	public void testSchedulePeriodically() throws Exception
	{
		@SuppressWarnings("unchecked")
		CompletableFuture<Boolean>[] futures = new CompletableFuture[3];
		for (int i = 0; i < futures.length; i++)
			futures[i] = new CompletableFuture<>();

		AtomicInteger k = new AtomicInteger(0);
		Scheduler scheduler = new Scheduler("ExceptionTest", null, HANDLER);
		scheduler.activate();
		ITask task = scheduler.schedulePeriodically(() ->
		{
			int l = k.getAndIncrement();
			if (l < futures.length)
				futures[l].complete(true);
		}, 0, 500, TimeUnit.MILLISECONDS);
		for (int i = 0; i < futures.length; i++)
			assertTrue(futures[i].get(1000, TimeUnit.MILLISECONDS).booleanValue());
		task.cancel();
	}

	@Test
	public void testSchedulePeriodicallyAndCancel() throws Exception
	{
		@SuppressWarnings("unchecked")
		CompletableFuture<Boolean>[] futures = new CompletableFuture[20];
		for (int i = 0; i < futures.length; i++)
			futures[i] = new CompletableFuture<>();

		AtomicInteger k = new AtomicInteger(0);
		Scheduler scheduler = new Scheduler("ExceptionTest", null, HANDLER);
		scheduler.activate();
		ITask task = scheduler.schedulePeriodically(() ->
		{
			int l = k.getAndIncrement();
			if (l < futures.length)
				futures[l].complete(true);
		}, 0, 500, TimeUnit.MILLISECONDS);
		for (int i = 0; i < 3; i++)
			assertTrue(futures[i].get(1000, TimeUnit.MILLISECONDS).booleanValue());
		task.cancel();
		for (int i = 3; i < futures.length; i++)
			if (!futures[i].getNow(Boolean.FALSE))
				return;
		fail();
	}

	@Test
	public void testScheduleAndShutdownExecutor() throws Exception
	{
		Scheduler scheduler = new Scheduler(null, null, HANDLER);
		scheduler.activate();
		AtomicBoolean bool = new AtomicBoolean(false);
		scheduler.schedule(() -> { bool.set(true); }, 3000, TimeUnit.MILLISECONDS);
		scheduler.schedulePeriodically(() -> { bool.set(true); }, 3000, 1000, TimeUnit.MILLISECONDS);
		scheduler.deactivate();
		Thread.sleep(4000);
		assertFalse(bool.get());
	}

}
