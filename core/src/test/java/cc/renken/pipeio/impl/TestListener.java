/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

import cc.renken.pipeio.IListener;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class TestListener implements IListener
{
	
	public final static class Event
	{
		
		public final EventType type;
		public final Object payload;
		
		private Event(EventType type, Object payload)
		{
			this.type = type;
			this.payload = payload;
		}
		
		@Override
		public String toString()
		{
			return this.type + ":" + String.valueOf(this.payload);
		}
	}
	
	
	private final LinkedBlockingDeque<Event> events = new LinkedBlockingDeque<>();


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void handleEvent(EventType type, Object payload)
	{
		this.events.add(new Event(type, payload));
	}
	
	public Event pollEvent()
	{
		try
		{
			return this.events.poll(1000, TimeUnit.MILLISECONDS);
		}
		catch (InterruptedException e)
		{
			throw new RuntimeException(e);
		}
	}

}
