/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.impl.Scheduler;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class TestScheduler extends Scheduler
{

	/**
	 * @param name
	 * @param threadGroup
	 * @param exceptionHandler
	 */
	public TestScheduler(IExceptionHandler exceptionHandler)
	{
		super("test", "test", exceptionHandler);
	}

	
	@Override
	public boolean isMe()
	{
		return true;
	}

}