/**
 * 
 */
package cc.renken.pipeio.impl;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.ConfigurationBuilder;

/**
 * @author renkenh
 *
 */
public class ComponentContainerTest
{

	private static final TestComponentContainer<String, String, String, String> createSingleContainer()
	{
		return createSingleContainer(ComponentContainerTest::exceptionHandle);
	}
	
	private static final TestComponentContainer<String, String, String, String> createSingleContainer(IExceptionHandler exceptionHandler)
	{
		TestScheduler scheduler = new TestScheduler(ComponentContainerTest::exceptionHandle);
		TestComponentContainer<String, String, String, String> next = new TestComponentContainer<>(scheduler, new TestComponent<>(), null, exceptionHandler);
		new PreviousTestComponentContainer<>(scheduler, new TestComponent<>(), next, exceptionHandler);
		return next;
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#AComponentContainer(cc.renken.pipeio.impl.Scheduler, cc.renken.pipeio.IComponent, cc.renken.pipeio.impl.AComponentContainer, cc.renken.pipeio.IExceptionHandler)}.
	 */
	@Test
	public void testAComponentContainer()
	{
		createSingleContainer();
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#notifyActiveStateChanged()}.
	 */
	@Test(expected=IllegalStateException.class)
	public void testNotifyActiveStateChangedException()
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		container.notifyActiveStateChanged();
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#notifyActiveStateChanged()}.
	 */
	@Test
	public void testNotifyActiveStateChanged() throws IOException, TimeoutException
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		container.activate(ConfigurationBuilder.withoutModel().compile());
		container.notifyActiveStateChanged();
		container.getComponent().setActive(false);
		container.notifyActiveStateChanged();
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#getNextState()}.
	 */
	@Test
	public void testIsNextActive()
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertEquals(null, container.getNextState());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#getScheduler()}.
	 */
	@Test
	public void testGetScheduler()
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertNotNull(container.getScheduler());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#exceptionEncountered(java.lang.Exception)}.
	 */
	@Test
	public void testExceptionEncountered()
	{
		AtomicBoolean b = new AtomicBoolean(false);
		TestComponentContainer<String, String, String, String> container = createSingleContainer((e) -> b.set(true));
		container.exceptionEncountered(new Exception());
		assertTrue(b.get());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#previous()}.
	 */
	@Test
	public void testPrevious()
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertNotNull(container.previous());
		assertTrue(container.hasPrevious());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#next()}.
	 */
	@Test
	public void testNext()
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertNull(container.next());
		assertFalse(container.hasNext());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#isHandlerActive()}.
	 */
	@Test
	public void testIsHandlerActive() throws IOException, TimeoutException
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertFalse(container.isHandlerActive());
		container.activate(ConfigurationBuilder.withoutModel().compile());
		assertTrue(container.isHandlerActive());
		container.activate(ConfigurationBuilder.withoutModel().compile());
		assertTrue(container.isHandlerActive());
		container.deactivate();
		assertFalse(container.isHandlerActive());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#getState()}.
	 */
	@Test
	public void testIsActive() throws IOException, TimeoutException
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertEquals(State.DEACTIVATED, container.getState());
		container.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, container.getState());
		container.getComponent().setActive(false);
		assertEquals(State.INACTIVE, container.getState());
		container.getComponent().setActive(true);
		assertEquals(State.ACTIVE, container.getState());
		container.deactivate();
		assertEquals(State.DEACTIVATED, container.getState());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#activate()}.
	 */
	@Test
	public void testDeActivate() throws IOException, TimeoutException
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		assertEquals(State.DEACTIVATED, container.getState());
		container.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, container.getState());
		container.activate(ConfigurationBuilder.withoutModel().compile());
		assertEquals(State.ACTIVE, container.getState());
		container.deactivate();
		assertEquals(State.DEACTIVATED, container.getState());
	}

	/**
	 * Test method for {@link cc.renken.pipeio.impl.AComponentContainer#updateLastState()}.
	 */
	@Test
	public void testUpdateLastIsActive() throws IOException, TimeoutException
	{
		TestComponentContainer<String, String, String, String> container = createSingleContainer();
		container.activate(ConfigurationBuilder.withoutModel().compile());
		assertFalse(container.getUpdateLastIsActive());
		container.getComponent().setActive(false, false);
		assertTrue(container.getUpdateLastIsActive());
		container.getComponent().setActive(true, false);
		assertTrue(container.getUpdateLastIsActive());
		container.deactivate();
		assertFalse(container.getUpdateLastIsActive());
	}


	private static void exceptionHandle(Exception e)
	{
		fail("Exception should not happen.");
	}

}
