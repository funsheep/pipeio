/**
 * 
 */
package cc.renken.pipeio.impl;

import java.io.IOException;

import cc.renken.pipeio.IExceptionHandler;
import cc.renken.pipeio.impl.AComponentContainer;
import cc.renken.pipeio.impl.Scheduler;

/**
 * @author renkenh
 *
 */
public class TestComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> extends AComponentContainer<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT, TestComponent<RECV_OUT,PUSH_OUT>>
{

	/**
	 * @param scheduler
	 * @param component
	 * @param next
	 * @param exceptionHandler
	 */
	public TestComponentContainer(Scheduler scheduler, TestComponent<RECV_OUT, PUSH_OUT> component, AComponentContainer<?, RECV_IN, PUSH_OUT, ?, ?> next, IExceptionHandler exceptionHandler)
	{
		super(scheduler, component, next, exceptionHandler);
		component.setup(this);
	}


	@Override
	protected void pushIn(Object entity) throws IOException
	{
		
	}

	@Override
	protected void receiveIn(Object entity)
	{
		// TODO Auto-generated method stub

	}

	public final boolean getUpdateLastIsActive()
	{
		return super.updateLastState();
	}

	
	public TestComponent<RECV_OUT, PUSH_OUT> getComponent()
	{
		return this.component;
	}
}
