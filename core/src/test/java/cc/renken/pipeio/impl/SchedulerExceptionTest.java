/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import cc.renken.pipeio.IScheduler.FailingRunnable;
import cc.renken.pipeio.IScheduler.ITask;
import cc.renken.pipeio.impl.Scheduler;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class SchedulerExceptionTest
{

	@Test
	public void testExceptionInScheduler() throws Exception
	{
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		Scheduler scheduler = new Scheduler("ExceptionTest", null, (ex) -> future.complete(Boolean.TRUE));
		scheduler.activate();
		scheduler.submit(() -> { throw new Exception("TestException");});
		assertTrue(future.get(5000, TimeUnit.MILLISECONDS).booleanValue());
	}

	@Test
	public void testPeriodicalExceptionInScheduler() throws Exception
	{
		@SuppressWarnings("unchecked")
		CompletableFuture<Boolean>[] futures = new CompletableFuture[3];
		for (int i = 0; i < futures.length; i++)
			futures[i] = new CompletableFuture<>();

		AtomicInteger k = new AtomicInteger(0);
		Scheduler scheduler = new Scheduler("ExceptionTest", null, (ex) -> futures[k.getAndIncrement()].complete(Boolean.TRUE));
		scheduler.activate();
		ITask task = scheduler.schedulePeriodically(() -> { throw new Exception("TestException");}, 0, 500, TimeUnit.MILLISECONDS);
		for (int i = 0; i < futures.length; i++)
			assertTrue(futures[i].get(1000, TimeUnit.MILLISECONDS).booleanValue());
		task.cancel();
	}

	@Test(expected=ExecutionException.class)
	public void testWaitWithExceptionRunnableScheduler() throws Exception
	{
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		FailingRunnable r = () -> { System.out.println("failing now"); throw new Exception("TestException");};
		Scheduler scheduler = new Scheduler("ExceptionTest", null, (ex) -> future.complete(Boolean.TRUE));
		scheduler.activate();
		scheduler.waitForExec(r);
		assertTrue(future.getNow(Boolean.FALSE).booleanValue());
	}

	@Test(expected=ExecutionException.class)
	public void testWaitWithExceptionCallableScheduler() throws Exception
	{
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		Callable<Void> c = () -> { throw new Exception("TestException");};
		Scheduler scheduler = new Scheduler("ExceptionTest", null, (ex) -> future.complete(Boolean.TRUE));
		scheduler.activate();
		scheduler.waitForExec(c);
		assertTrue(future.getNow(Boolean.FALSE).booleanValue());
	}

	@Test(expected=ExecutionException.class)
	public void testWaitWithExceptionCallableIsMeScheduler() throws Exception
	{
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		Callable<Void> c = () -> { throw new Exception("TestException");};
		Scheduler scheduler = new Scheduler("ExceptionTest", null, (ex) -> future.complete(Boolean.TRUE))
		{
			@Override
			public boolean isMe()
			{
				return true;
			}
		};
		scheduler.activate();
		scheduler.waitForExec(c);
		assertTrue(future.getNow(Boolean.FALSE).booleanValue());
	}

	@Test(expected=RejectedExecutionException.class)
	public void testInactiveSchedulerSubmit() throws Exception
	{
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		FailingRunnable r = () -> { throw new Exception("TestException");};
		Scheduler scheduler = new Scheduler("ExceptionTest", null, (ex) -> future.complete(Boolean.TRUE))
		{
			@Override
			public boolean isMe()
			{
				return true;
			}
		};
		scheduler.submit(r);
		fail();
	}

}
