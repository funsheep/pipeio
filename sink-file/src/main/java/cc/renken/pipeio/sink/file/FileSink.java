/**
 * 
 */
package cc.renken.pipeio.sink.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.config.AtProperties.Property;
import cc.renken.pipeio.impl.AAsyncComponent;

/**
 * @author renkenh
 *
 */
@Property(name=FileSink.PROP_PATH, isRequired=true, description="Path to a file to which this sink should write the pushed data.")
@Property(name=FileSink.PROP_OPERATION, value="NEW", validationModel="enum:cc.renken.pipeio.async.sink.file.Operation", description="Specifies whether the file should be created on activation or data should be appended.")
public class FileSink extends AAsyncComponent<byte[], Void> implements IAsyncSink<byte[], byte[]>
{
	
	public static final String PROP_OPERATION = "operation";
	public static final String PROP_PATH = "path";
	
	public enum Operation
	{
		NEW,
		APPEND
	}

	
	private Path path;

	private boolean isActive = false;

	
//	public FileSink(String filename)
//	{
//		this(filename, Operation.NEW);
//	}
//	
//	public FileSink(String filename, Operation operation)
//	{
//		this(Paths.get(filename), operation);
//	}
//	
//	public FileSink(Path path)
//	{
//		this(path, Operation.NEW);
//	}
//	
//	public FileSink(Path path, Operation operation)
//	{
//		this.path = path;
//		this.operation = operation;
//	}
//

	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
		Operation op = config.getEnumEntry(PROP_OPERATION);
		this.path = Paths.get(config.getStringValue(PROP_PATH));
		if (op == Operation.NEW && Files.exists(this.path))
		{
			try
			{
				Files.delete(this.path);
			}
			catch (IOException e)
			{
				//die silently
			}
		}
	}

	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public State getState()
	{
		if (!this.isActive)
			return State.DEACTIVATED;
		if (!Files.isWritable(this.path))
			return State.INACTIVE;
		return State.ACTIVE;
	}

	@Override
	public void deactivate()
	{
		this.isActive = false;
		this.path = null;
	}

	@Override
	public void push(byte[] entity) throws IOException
	{
		if (this.getState() != State.ACTIVE)
			throw new IOException("File not available.");
		
		Files.write(this.path, entity, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
	}


}
