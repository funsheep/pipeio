/*
	This file is part of the javaappplatform network library.
	Copyright (C) 2013 funsheep
	Copyright (C) 2005-2013 d3fact Project Team (www.d3fact.de)
	
	This library is subject to the terms of the Mozilla Public License, v. 2.0.
	You should have received a copy of the MPL along with this library; see the 
	file LICENSE. If not, you can obtain one at http://mozilla.org/MPL/2.0/.
*/
package cc.renken.pipeio.sink.tcp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import cc.renken.pipeio.util.ARestartableRunner;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class TCPPing extends ARestartableRunner
{
	

	private final TCPSink sink;
	private final InetAddress address;
	private final int timeout;
	private final long interval;


	/**
	 * 
	 */
	public TCPPing(InetSocketAddress address, int interval, int timeout, TCPSink sink)
	{
		this.address = address.getAddress();
		this.timeout = timeout;
		this.interval = interval;
		this.sink = sink;
	}


	@Override
	protected void run()
	{
		final long start = System.currentTimeMillis();
		boolean isReachable = true;
		try
		{
			isReachable = this.address.isReachable(this.timeout);
		}
		catch (IOException e)
		{
			isReachable = false;
		}
		this.sink.setReachable(isReachable);
		final long diff = this.interval - (System.currentTimeMillis() - start);
		if (diff > 0)
			try
			{
				Thread.sleep(diff);
			}
			catch (InterruptedException e)
			{
				//may be interrupted for closing down this thread
			}
	}

}
