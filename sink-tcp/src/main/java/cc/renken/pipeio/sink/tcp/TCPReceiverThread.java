/**
 * 
 */
package cc.renken.pipeio.sink.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import cc.renken.pipeio.util.ARestartableRunner;

/**
 * @author renkenh
 *
 */
class TCPReceiverThread extends ARestartableRunner
{
	
	private final TCPSink sink;

	private final byte[] readBuffer = new byte[512];
	private InputStream stream;


	/**
	 * 
	 */
	public TCPReceiverThread(TCPSink sink)
	{
		this.sink = sink;
	}
	
	
	@Override
	public void start()
	{
		throw new UnsupportedOperationException("use start(Socket) instead.");
	}
	
	void start(Socket socket) throws IOException
	{
		this.stream = socket.getInputStream();
		super.start();
	}
	
	@Override
	protected void run()
	{
		try
		{
			int read = this.stream.read(this.readBuffer);
			if (read == -1)
				return;
			if (read > 0)
			{
				byte[] print = new byte[read];
				System.arraycopy(this.readBuffer, 0, print, 0, print.length);
				if (this.isRunning())
					this.sink.pushToPrevious(print);
			}
		}
		catch (Exception ex)
		{
			if (this.isRunning())
				this.sink.handleException(ex);
		}
	}

}
