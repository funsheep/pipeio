/**
 * 
 */
package cc.renken.pipeio.sink.tcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.config.AtProperties.Property;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.impl.AAsyncSink;

/**
 * FIXME should monitor and recover itself - through integrated scheduler
 * FIXME should ping the address and update state - use extra thread since this action is a blocking action, e.g. use a scheduler with a custom thread factory (daemonize)
 * 
 * @author renkenh
 *
 */
@Property(name=TCPSink.PROP_HOST, isRequired=true, validationModel="string")
@Property(name=TCPSink.PROP_PORT, isRequired=true, validationModel="int:0...65535")
@Property(name=TCPSink.PROP_PING, isRequired=false, value="4000", validationModel="int:0...")
public class TCPSink extends AAsyncSink<byte[], byte[]> implements IAsyncSink<byte[], byte[]>
{
	
	public static final String PROP_HOST = "host";
	public static final String PROP_PORT = "port";
	public static final String PROP_PING = "ping";
	
	
	private TCPPing ping;
	private Socket socket = null;
	private TCPReceiverThread receiver;
	private boolean isReachable = true;


	@Override
	protected void connect(Configuration configuration) throws IOException
	{
		String host = configuration.getStringValue(PROP_HOST);
		int port = configuration.getIntValue(PROP_PORT);
		int ping = configuration.getIntValue(PROP_PING);
		InetSocketAddress address = new InetSocketAddress(host, port);
		int timeout = configuration.getIntValue(IComponent.PROP_TIMEOUT);
		if (ping > 0)
			this.ping = new TCPPing(address, configuration.getIntValue(PROP_PING), timeout, this);
		
		this.socket = Utils.connectTo(address, timeout);
		this.receiver = new TCPReceiverThread(this);
		this.receiver.start(this.socket);
	}

	@Override
	public void push(byte[] entity) throws IOException
	{
		try
		{
			this.socket.getOutputStream().write(entity);
			this.socket.getOutputStream().flush();
		}
		catch (IOException ex)
		{
			this.disconnect();
			throw ex;
		}
	}

	//FIXME implement leaking counter for reachable
	protected void setReachable(boolean isReachable)
	{
		this.isReachable = isReachable;
		this.handler().notifyActiveStateChanged();
	}

	@Override
	protected boolean isConnected()
	{
		return (this.ping == null || this.isReachable) && this.socket != null && this.socket.isConnected();
	}

	@Override
	protected void disconnect()
	{
		Utils.close(this.socket);
		Utils.close(this.receiver);
		Utils.close(this.ping);
		this.socket = null;
		this.receiver = null;
		this.ping = null;
		this.isReachable = true;
		this.handler().notifyActiveStateChanged();
	}

	void pushToPrevious(byte[] print)
	{
		this.handler().getScheduler().submit(() -> this.handler().pushToPrevious(print));
	}

	void handleException(Exception ex)
	{
		this.handler().getScheduler().submit(() ->
		{
			this.disconnect();
			this.handler().exceptionEncountered(ex);
		});
	}

}
