/**
 * 
 */
package cc.renken.pipeio.sink.tcp;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author renkenh
 *
 */
class Utils
{

	/**
	 * Convenient method to create a socket with specific parameters.
	 * This method especially sets
	 * {@link Socket#setKeepAlive(boolean)} to true
	 * {@link Socket#setSoLinger(boolean, int)} to true with the given timeout
	 * {@link Socket#setSoTimeout(int)} to the given timeout.
	 * @param host The host to connect to.
	 * @param port The port on which to connect.
	 * @param timeout The timeout used for the different settings (see above).
	 * @return A socket that is connected.
	 * @throws IOException If the socket could not be created or connected.
	 */
	static final Socket connectTo(String host, int port, int timeout) throws IOException
	{
		return connectTo(new InetSocketAddress(host, port), timeout);
	}
	
	static final Socket connectTo(InetSocketAddress address, int timeout) throws IOException
	{
		Socket socket = new Socket();
		socket.setKeepAlive(true);
		socket.setSoLinger(true, timeout);
		socket.setSoTimeout(timeout);
		socket.connect(address, timeout);
		return socket;
	}

	static final void close(Closeable close)
	{
		try
		{
			if (close != null)
				close.close();
		}
		catch (IOException ex)
		{
			//die silently
		}
	}
}
