# pipeio #

This is a library for communication. It follows more or less the same principle as netty.io. However, netty.io is more complex and requires that certain rules
have to be followed. While netty.io therefore may be more efficient and provides more features, this library is pretty simple, easy to maintain and therefore
very robust.

## Architecture ##

The idea behind pipeline constructs like netty.io and this one are, that both the communication protocol and features like health checks, self-healing, etc. can be separated into
independent small structures of code. This makes a complex communication protocol easier to implement and maintain. For example, these structures can be easily
replaced by rewritten components, different protocols can share the same structures, etc.

Therefore, this pipeline starts with a faucet (or tab) which is connected to several tubes which (in the end) are connected to a sink. Data that has to be send to
a communication partner is pushed from the faucet down to the sink while it visits each tube which may transform the data along the way. For received data, its
the other way round.

Each of the components is embedded into a container. That means, components are never directly attached to each other. Instead, they are kept separated. This simplifies
the thinking and implementation. The containers have been made robust especially against unexpected errors. 

## Start ##

First an `IFaucet` has to be implemented. This component is the interface to the application logic and is left without any predefined methods intentionally. This allows to
implement a perfect fitting API for the application logic. After that being done, `ITube`s have to be implemented if needed. `ITube`s can implement a lot of different functionality
like data transformation, self-healing, logging, health check functionalities. The needed transportation layer like TCP/IP, RS232, Files, etc. has to be wrapped in `ISink`-implementations.
Several ready to use sinks are shipped with this library.

## Putting a Pipeline together ##

The `Flow` component uses a builder pattern to simplify pipeline building. E.g. for the unit-tests the following line of code is used:

```
IPipeline<String, String> pipe = Flow.from(new TestFaucet<>()).through(new TestTube<>()).through(new TestTube<>()).into(new TestSink<>()).byId("Hello");
```
