/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IPipeline;
import cc.renken.pipeio.ISyncPushFaucet;
import cc.renken.pipeio.ISyncReceiveFaucet;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.config.PropertyModel;
import cc.renken.pipeio.config.PropertyModelBuilder;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
class SyncPipeline<RECV, PUSH> extends APipeline<RECV, PUSH> implements IPipeline<RECV, PUSH>
{
	
	private final AsyncFaucetContainer<RECV, PUSH> asyncFaucetContainer;
	private final LinkedList<AsyncTubeContainer<?, ?, ?, ?>> partHandlers = new LinkedList<>();
	private final AsyncSinkContainer<?, ?> asyncSinkContainer;
	private final PropertyModel propertyModel;


	/**
	 * Constructor. Creates a new pipeline without an Id, i.e. {@link #id()} will return <code>null</code>.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	public SyncPipeline(ISyncPushFaucet<RECV,PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		this(null, faucet, parts, sink);
	}
	
	/**
	 * Constructor. Creates a new pipeline without an Id, i.e. {@link #id()} will return <code>null</code>.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	public SyncPipeline(ISyncReceiveFaucet<RECV,PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		this(null, faucet, parts, sink);
	}
	
	/**
	 * Constructor. Creates a new pipeline with an id. The id is used throughout the pipeline to identify threads, components and errors.
	 * @param id The id of the pipeline. May be null.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	public SyncPipeline(String id, ISyncPushFaucet<RECV,PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		this(id, faucet, true, parts, sink);
	}

	/**
	 * Constructor. Creates a new pipeline with an id. The id is used throughout the pipeline to identify threads, components and errors.
	 * @param id The id of the pipeline. May be null.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	public SyncPipeline(String id, ISyncReceiveFaucet<RECV,PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		this(id, faucet, false, parts, sink);
	}

	/**
	 * Constructor. Creates a new pipeline with an id. The id is used throughout the pipeline to identify threads, components and errors.
	 * @param id The id of the pipeline. May be null.
	 * @param faucet The faucet to be used by this pipeline.
	 * @param parts The parts that make up the rest of the pipeline.
	 * @param sink The sink.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private SyncPipeline(String id, Object faucet, boolean push, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		super(id);
		this.asyncSinkContainer = new AsyncSinkContainer<>(this.scheduler(), sink, this::exceptionEncounteredNotifyListeners);
		AAsyncComponentContainer<?, ?, ?, ?, ?> next = this.asyncSinkContainer;

		List<IAsyncTube<?, ?, ?, ?>> partsRevs = new ArrayList<>(parts);
		Collections.reverse(partsRevs);
		for (IAsyncTube<?, ?, ?, ?> tube : partsRevs)
		{
			//use rawtypes here, since we do not care about types here anymore - this is only needed during building of the pipeline
			AsyncTubeContainer handler = new AsyncTubeContainer(this.scheduler(), tube, next, this::exceptionEncounteredNotifyListeners);
			this.partHandlers.addFirst(handler);
			next = handler;
		}
		
		AAsyncComponentContainer<?, RECV, PUSH, ?, ? extends IComponent<?, ?>> firstHandler = (AAsyncComponentContainer<?, RECV, PUSH, ?, ? extends IComponent<?,?>>) (!this.partHandlers.isEmpty() ? this.partHandlers.getFirst() : this.asyncSinkContainer);
		SyncFaucetContainerAdapter<RECV, PUSH> faucetAdapter = push ? new SyncFaucetContainerAdapter<>((ISyncPushFaucet<RECV, PUSH>) faucet) : new SyncFaucetContainerAdapter<>((ISyncReceiveFaucet<RECV, PUSH>) faucet);
		this.asyncFaucetContainer = new AsyncFaucetContainer<>(this, faucetAdapter, firstHandler, this::exceptionEncounteredNotifyListeners);
		
		PropertyModelBuilder pmBuilder = PropertyModelBuilder.create();
		pmBuilder.addPropertiesFromType(faucet.getClass());
		for (IAsyncTube<?, ?, ?, ?> tube : partsRevs)
			pmBuilder.addPropertiesFromType(tube.getClass());
		pmBuilder.addPropertiesFromType(sink.getClass());
		this.propertyModel = pmBuilder.compile();
	}


	@Override
	protected void activateFaucet(Configuration config) throws IOException, TimeoutException
	{
		this.asyncFaucetContainer.activateFaucet(config);
	}

	@Override
	protected State getFaucetState()
	{
		return this.asyncFaucetContainer.getFaucetState();
	}

	@Override
	protected void deactivateFaucet()
	{
		this.asyncFaucetContainer.deactivateFaucet();
	}
	
	@Override
	protected Scheduler scheduler()
	{
		return super.scheduler();
	}

	@Override
	protected final void stateChangedNotifyListeners() throws ExecutionException, TimeoutException
	{
		super.stateChangedNotifyListeners();
	}
	
	@Override
	protected final void receivedNotifyListeners(RECV entity)
	{
		super.receivedNotifyListeners(entity);
	}

	@Override
	protected final void sendNotifyListeners(PUSH entity)
	{
		super.sendNotifyListeners(entity);
	}

	@Override
	protected final void exceptionEncounteredNotifyListeners(Exception ex)
	{
		super.exceptionEncounteredNotifyListeners(ex);
	}

	@Override
	public PropertyModel getConfigurationModel()
	{
		return this.propertyModel;
	}

}
