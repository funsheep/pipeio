/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncFaucet;
import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IScheduler;
import cc.renken.pipeio.ISyncPushFaucet;
import cc.renken.pipeio.ISyncPushFaucetContainer;
import cc.renken.pipeio.ISyncReceiveFaucet;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;


/**
 * Adapter that can handle either {@link ISyncPushFaucet} or {@link ISyncReceiveFaucet}.
 * @author Hendrik Renken
 */
final class SyncFaucetContainerAdapter<RECV, PUSH> implements ISyncPushFaucetContainer<RECV, PUSH>, IAsyncFaucet<RECV, PUSH>
{

	private IAsyncComponentContainer<Void, PUSH> handler;
	private final IComponent<RECV, PUSH> genericFaucet;
	private ISyncPushFaucet<RECV, PUSH> pushFaucet;
	private ISyncReceiveFaucet<RECV, PUSH> recvFaucet;
	
	
	public SyncFaucetContainerAdapter(ISyncPushFaucet<RECV, PUSH> faucet)
	{
		this.pushFaucet = faucet;
		this.genericFaucet = faucet;
	}
	
	public SyncFaucetContainerAdapter(ISyncReceiveFaucet<RECV, PUSH> faucet)
	{
		this.recvFaucet = faucet;
		this.genericFaucet = faucet;
	}
	

	@Override
	public void setup(IAsyncComponentContainer<Void, PUSH> handler)
	{
		this.handler = handler;
		if (this.pushFaucet != null)
		{
			this.pushFaucet.setup(this);
			return;
		}
		this.recvFaucet.setup(this);
	}


	@Override
	public void activateNext() throws IOException, TimeoutException
	{
		this.handler.activateNext();
	}

	@Override
	public void notifyActiveStateChanged()
	{
		this.handler.notifyActiveStateChanged();
	}

	@Override
	public State getNextState()
	{
		return this.handler.getNextState();
	}

	@Override
	public void deactivateNext()
	{
		this.handler.deactivateNext();
	}

	@Override
	public IScheduler getScheduler()
	{
		return this.handler.getScheduler();
	}

	@Override
	public void exceptionEncountered(Exception ex)
	{
		this.handler.exceptionEncountered(ex);
	}

	@Override
	public void activate(Configuration config) throws IOException, TimeoutException
	{
		this.genericFaucet.activate(config);
	}

	@Override
	public void activeStateChanged()
	{
		this.genericFaucet.activeStateChanged();
	}

	@Override
	public State getState()
	{
		return this.genericFaucet.getState();
	}

	@Override
	public void deactivate()
	{
		this.genericFaucet.deactivate();
	}
	
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition entityReceived = this.lock.newCondition();
	private boolean awaiting = false;
	private RECV receivedEntity;
	

	@Override
	public void receive(RECV entity)
	{
		this.lock.lock();
		try
		{
			if (this.pushFaucet != null)
			{
				this.handleReceivePushFaucet(entity);
				return;
			}
			this.handleReceiveRecvFaucet(entity);
		}
		finally
		{
			this.lock.unlock();
		}
	}

	private final void handleReceiveRecvFaucet(RECV entity)
	{
		PUSH push = this.recvFaucet.receive(entity);
		try
		{
			this.handler.pushToNext(push);
		}
		catch (IOException | TimeoutException e)
		{
			this.exceptionEncountered(e);
		}
	}

	private final void handleReceivePushFaucet(RECV entity)
	{
		if (!this.awaiting)
		{
			this.exceptionEncountered(new IllegalStateException("Received data "+entity+" while no one was expecting it."));
			return;
		}
		
		this.receivedEntity = entity;
		this.entityReceived.signal();
	}

	/** This method is called by PushFaucet only. */
	@Override
	public RECV pushToNext(PUSH entity) throws IOException, TimeoutException
	{
		this.lock.lock();
		try
		{
			this.awaiting = true;
			this.handler.pushToNext(entity);
			if (!this.entityReceived.await(10000, TimeUnit.MILLISECONDS))	//FIXME make timeout value dynamic
				throw new TimeoutException("Didn't get response for " + entity + " within time limit.");
			return this.receivedEntity;
		}
		catch (InterruptedException e)
		{
			throw new IOException(e);
		}
		finally
		{
			this.awaiting = false;
			this.lock.unlock();
		}
	}

}
