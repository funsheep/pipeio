/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.util.LinkedHashSet;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IAsyncTube;
import cc.renken.pipeio.IPipeline;
import cc.renken.pipeio.ISyncPushFaucet;
import cc.renken.pipeio.ISyncReceiveFaucet;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public final class SyncFlow<RECV, PUSH>
{

	public static final class Flowing<RECV_OUT, PUSH_IN, RECV, PUSH>
	{
		
		private final SyncFlow<RECV, PUSH> syncFlow;
		

		private Flowing(SyncFlow<RECV, PUSH> flow)
		{
			this.syncFlow = flow;
		}

		
		public final <RECV_IN, PUSH_OUT> Flowing<RECV_IN, PUSH_OUT, RECV, PUSH> through(IAsyncTube<RECV_IN, RECV_OUT, PUSH_IN, PUSH_OUT> part)
		{
			this.syncFlow.parts.add(part);
			return new Flowing<>(this.syncFlow);
		}
		
		public final Finishing<RECV, PUSH> into(IAsyncSink<RECV_OUT, PUSH_IN> sink)
		{
			this.syncFlow.asyncSink = sink;
			return new Finishing<>(this.syncFlow);
		}
	}
	
	public static final class Finishing<RECV, PUSH>
	{
		
		private final SyncFlow<RECV, PUSH> syncFlow;

		
		private Finishing(SyncFlow<RECV, PUSH> flow)
		{
			this.syncFlow = flow;
		}
		
		public final IPipeline<RECV, PUSH> byId(String id)
		{
			SyncPipeline<RECV, PUSH> pipe = this.syncFlow.pushFaucet != null ? new SyncPipeline<>(id, this.syncFlow.pushFaucet, this.syncFlow.parts, this.syncFlow.asyncSink) : new SyncPipeline<>(id, this.syncFlow.recvFaucet, this.syncFlow.parts, this.syncFlow.asyncSink);
			return pipe;
		}
		
		public final IPipeline<RECV, PUSH> compile()
		{
			SyncPipeline<RECV, PUSH> pipe = this.syncFlow.pushFaucet != null ? new SyncPipeline<>(this.syncFlow.pushFaucet, this.syncFlow.parts, this.syncFlow.asyncSink) : new SyncPipeline<>(this.syncFlow.recvFaucet, this.syncFlow.parts, this.syncFlow.asyncSink);
			return pipe;
		}
	}


	private final LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts = new LinkedHashSet<>();
	private final ISyncPushFaucet<RECV, PUSH> pushFaucet;
	private final ISyncReceiveFaucet<RECV, PUSH> recvFaucet;
	private IAsyncSink<?, ?> asyncSink;

	
	private SyncFlow(ISyncPushFaucet<RECV, PUSH> faucet)
	{
		this.pushFaucet = faucet;
		this.recvFaucet = null;
	}

	private SyncFlow(ISyncReceiveFaucet<RECV, PUSH> faucet)
	{
		this.recvFaucet = faucet;
		this.pushFaucet = null;
	}


	public static final <RECV, PUSH> Flowing<RECV, PUSH, RECV, PUSH> from(ISyncPushFaucet<RECV, PUSH> faucet)
	{
		final SyncFlow<RECV, PUSH> flow = new SyncFlow<>(faucet);
		return new Flowing<>(flow);
	}
	
	public static final <RECV, PUSH> Flowing<RECV, PUSH, RECV, PUSH> from(ISyncReceiveFaucet<RECV, PUSH> faucet)
	{
		final SyncFlow<RECV, PUSH> flow = new SyncFlow<>(faucet);
		return new Flowing<>(flow);
	}
	
}
