/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package cc.renken.pipeio;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IComponentContainer;

/**
 * This type is the interface that the pipeline provides to the {@link IComponent}. It allows sending data down to the {@link IAsyncSink} by using
 * {@link #pushToNext(Object)} and back up the pipe to the {@link ISyncReceiveFaucet} by using {@link #pushToPrevious(Object)}.
 * The component can ask for the current state of the subsequent pipeline, i.e. if it can send data to the communication partner by querying
 * {@link #getNextState()}.
 * 
 * Scheduling is available by using the scheduler provided by {@link #getScheduler()}.
 * 
 * In contrast to that {@link #notifyActiveStateChanged()} must be called by the component to notify about unexpected changes to its {@link IComponent#getState()}
 * state.
 * Furthermore, to notify about unexpected exceptions that the component cannot handle by itself, it should use {@link #exceptionEncountered(Exception)}
 * to notify the pipeline about that.
 * 
 * @author Hendrik Renken
 */
public interface ISyncPushFaucetContainer<RECV_OUT, PUSH_OUT> extends IComponentContainer<RECV_OUT, PUSH_OUT>
{
	
	/**
	 * A component must use this method to push data "down" the pipeline to the {@link IAsyncSink}. This method may be called only after a
	 * call to {@link IComponent#activate()} has happened and {@link #getNextState()} returns <code>true</code>.
	 * This method may not be called after a call to {@link IComponent#deactivate()}. 
	 * @param entity The entity to send down.
	 * 
	 * An exception is thrown when somewhere later in the pipeline an exception occurs. The component caller can itself decide what
	 * to do: Catch the exception and handle it (e.g. set my own status to inactive, send an entity up through the pipeline using
	 * {@link #pushToPrevious(Object)}, or do not catch it leaving this tasks to others.
	 * 
	 * @throws IOException is thrown when something happens and the push cannot be finished. E.g. the sink may have no connection to
	 * the communication partner.
	 */
	public RECV_OUT pushToNext(PUSH_OUT entity) throws IOException, TimeoutException;
	
}
