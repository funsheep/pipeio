/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio;

import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IComponentContainer;

/**
 * This type represents the first component in the pipeline. This type must be used to send data to the communication partner or {@link #receive(Object)} data
 * from it. The faucet is the main API for communication from other parts of a program. The user of this library must implement an appropriate API within this
 * component to be used by his logic.
 * 
 * The container that embeds this component is thread save. All calls to the container (and therefore to the pipeline) are executed by the pipeline
 * scheduler/thread. Also all calls from the pipeline to this component are also executed by this thread.
 * 
 * @author Hendrik Renken
 */
public interface ISyncReceiveFaucet<RECV, PUSH> extends IComponent<RECV, PUSH>
{

	/**
	 * This method is called during activation of the pipeline.
	 * @param handler The handler provides methods to interact with the pipeline and the previous and next component.
	 */
	public void setup(IComponentContainer<RECV, PUSH> handler);


	/**
	 * This method is called from the pipeline thread/scheduler whenever an entity is received from the subsequent parts of the pipeline.
	 * The faucet can/should react to this and process it further or send it appropriately to other parts of the user program.
	 * @param entity An entity to process in any way seen fit by this part.
	 */
	public PUSH receive(RECV entity);

}
