/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.sync;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.IPipeline;
import cc.renken.pipeio.async.LoggingTube;
import cc.renken.pipeio.config.ConfigurationBuilder;
import cc.renken.pipeio.impl.SyncFlow;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class SimpleReceiveDemo
{

	private static final ReentrantLock lock = new ReentrantLock();
	private static final Condition condition = lock.newCondition();


	public static final void main(String[] args) throws Exception
	{
		EchoFaucet faucet = new EchoFaucet();
		IPipeline<String, String> pipe = SyncFlow.from(faucet).through(new CounterTransformer()).through(new LoggingTube<>()).into(new StringGeneratorSink()).compile();
		pipe.addListener(new IListener()
		{
			
			@Override
			public void handleEvent(EventType type, Object entity)
			{
				switch (type)
				{
					case DATA_RECEIVED:
						System.out.println(entity);
						break;
					case EXCEPTION_OCCURRED:
						((Exception) entity).printStackTrace();
						break;
					case STATE_CHANGED:
						System.out.println(entity);
						break;
					default:
						break;
					
				}
			}
		});
		pipe.activate(ConfigurationBuilder.withoutModel().compile());
		lock.lock();
		try
		{
			condition.awaitUninterruptibly();
		}
		finally
		{
			lock.unlock();
		}
	}

}
