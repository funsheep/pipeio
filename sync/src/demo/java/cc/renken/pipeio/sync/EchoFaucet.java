/**
 * 
 */
package cc.renken.pipeio.sync;

import cc.renken.pipeio.IComponentContainer;
import cc.renken.pipeio.ISyncReceiveFaucet;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
public class EchoFaucet implements ISyncReceiveFaucet<String, String>
{

	@Override
	public void activate(Configuration config)
	{
		//do nothing
	}

	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public State getState()
	{
		return State.ACTIVE;
	}

	@Override
	public void deactivate()
	{
		//do nothing
	}

	@Override
	public void setup(IComponentContainer<String, String> handler)
	{
		//do nothing
	}

	@Override
	public String receive(String entity)
	{
		return "Response: " + entity;
	}

}
