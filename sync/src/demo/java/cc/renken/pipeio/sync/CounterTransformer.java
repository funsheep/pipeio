/**
 * 
 */
package cc.renken.pipeio.sync;

import cc.renken.pipeio.AAsyncTransformer;

/**
 * @author renkenh
 *
 */
public class CounterTransformer extends AAsyncTransformer<String, String, String, String>
{
	
	private int counter = 0;
	
	@Override
	public String transformPush(String entity)
	{
		return entity;
	}

	@Override
	public String transformReceive(String entity)
	{
		return entity + "[" + (counter++) + "]";
	}

}
