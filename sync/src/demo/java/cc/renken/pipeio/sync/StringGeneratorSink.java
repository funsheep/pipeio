/**
 * 
 */
package cc.renken.pipeio.sync;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IScheduler.ITask;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.impl.AAsyncSink;


/**
 * @author renkenh
 *
 */
public class StringGeneratorSink extends AAsyncSink<String, String>
{

	private ITask generator;
	
	
	private void gen()
	{
		this.handler().pushToPrevious("Hello from Sink");
	}
	
	@Override
	public void push(String entity) throws IOException
	{
		System.out.println("Received Response: " + entity);
	}

	@Override
	protected boolean isConnected()
	{
		return this.generator != null;
	}

	@Override
	protected void connect(Configuration configuration) throws IOException, TimeoutException
	{
		this.generator = this.handler().getScheduler().schedulePeriodically(this::gen, 1, 1, TimeUnit.SECONDS);
	}

	@Override
	protected void disconnect()
	{
		if (this.generator != null)
			this.generator.cancel();
		this.generator = null;
	}

}
