/**
 * 
 */
package cc.renken.pipeio.impl;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author renkenh
 *
 */
public class SyncTestSink<RECV, PUSH> extends AsyncTestSink<RECV, PUSH>
{
	
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition wait = this.lock.newCondition();

	
	@Override
	public void push(PUSH entity)
	{
		this.lock.lock();
		try
		{
			super.push(entity);
			this.wait.signal();
		}
		finally
		{
			this.lock.unlock();
		}
	}
	
	public PUSH gotPush()
	{
		this.lock.lock();
		try
		{
			PUSH push = super.gotPush();
			if (push != null)
				return push;
			
			if (!this.wait.await(100, TimeUnit.SECONDS))
				throw new TimeoutException();
			return super.gotPush();
		}
		catch (InterruptedException | TimeoutException e)
		{
			throw new RuntimeException(e);
		}
		finally
		{
			this.lock.unlock();
		}
	}

}
