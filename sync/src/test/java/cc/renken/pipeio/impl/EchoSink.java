/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;

import cc.renken.pipeio.IAsyncComponentContainer;
import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class EchoSink<TYPE> implements IAsyncSink<TYPE, TYPE>
{

	private IAsyncComponentContainer<TYPE, Void> componentContainer;
	private boolean isActive = false;
	private TYPE pushed = null;
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setup(IAsyncComponentContainer<TYPE, Void> handler)
	{
		this.componentContainer = handler;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void push(TYPE entity) throws IOException
	{
		if (!this.isActive)
			throw new IOException("Sink not ready.");
		this.pushed = entity;
		this.componentContainer.pushToPrevious(entity);
	}
	
	public TYPE gotPush()
	{
		return this.pushed;
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deactivate()
	{
		this.isActive = false;
	}


	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

}
