/* Copyright (c) 2017 Hendrik Renken
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
package cc.renken.pipeio.impl;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.ISyncPushFaucet;
import cc.renken.pipeio.ISyncPushFaucetContainer;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * TODO javadoc
 * @author Hendrik Renken
 */
public class TestPushFaucet<RECV, PUSH> implements ISyncPushFaucet<RECV, PUSH>
{

	private ISyncPushFaucetContainer<RECV, PUSH> componentContainer;
	private boolean isActive = false;


	@Override
	public void activate(Configuration config)
	{
		this.isActive = true;
	}

	@Override
	public State getState()
	{
		if (this.isActive)
			return State.ACTIVE;
		return State.DEACTIVATED;
	}

	@Override
	public void deactivate()
	{
		this.isActive = false;
	}

	@Override
	public void setup(ISyncPushFaucetContainer<RECV, PUSH> handler)
	{
		this.componentContainer = handler;
	}

	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	public RECV push(PUSH entity) throws IOException, TimeoutException
	{
		return this.componentContainer.pushToNext(entity);
	}

}
