/**
 * 
 */
package cc.renken.pipeio.impl;

import cc.renken.pipeio.IComponentContainer;
import cc.renken.pipeio.ISyncReceiveFaucet;
import cc.renken.pipeio.State;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
public class TestReceiveFaucet<RECV, PUSH> implements ISyncReceiveFaucet<RECV, PUSH>
{

	private boolean called = false;
	
	@Override
	public void activate(Configuration config)
	{
		//do nothing
	}

	@Override
	public void activeStateChanged()
	{
		//do nothing
	}

	@Override
	public State getState()
	{
		return State.ACTIVE;
	}

	@Override
	public void deactivate()
	{
		//do nothing
	}

	@Override
	public void setup(IComponentContainer<RECV, PUSH> handler)
	{
		//do nothing
	}

	@Override
	public final PUSH receive(RECV entity)
	{
		this.called = true;
		return this.transform(entity);
	}
	
	@SuppressWarnings("unchecked")
	protected PUSH transform(RECV entity)
	{
		return (PUSH) entity;
	}
	
	public boolean wasCalled()
	{
		return this.called;
	}

}
