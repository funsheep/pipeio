package cc.renken.pipeio.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import cc.renken.pipeio.IAsyncSink;
import cc.renken.pipeio.IAsyncTube;

public class SyncTestPipeline<RECV, PUSH> extends SyncPipeline<RECV, PUSH>
{
	
	private final TestPushFaucet<RECV,PUSH> pushFaucet;
	private final TestReceiveFaucet<RECV,PUSH> recvFaucet;
	private final List<IAsyncTube<?, ?, ?, ?>> parts;
	private final IAsyncSink<?, ?> asyncSink;
	private TestScheduler scheduler;


	public SyncTestPipeline(TestPushFaucet<RECV, PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		super(faucet, parts, sink);
		this.pushFaucet = faucet;
		this.recvFaucet = null;
		this.parts = new ArrayList<>(parts);
		this.asyncSink = sink;
	}

	public SyncTestPipeline(TestReceiveFaucet<RECV, PUSH> faucet, LinkedHashSet<IAsyncTube<?, ?, ?, ?>> parts, IAsyncSink<?, ?> sink)
	{
		super(faucet, parts, sink);
		this.pushFaucet = null;
		this.recvFaucet = faucet;
		this.parts = new ArrayList<>(parts);
		this.asyncSink = sink;
	}


	public TestPushFaucet<RECV,PUSH> getPushFaucet()
	{
		return this.pushFaucet;
	}
	
	public TestReceiveFaucet<RECV,PUSH> getRecvFaucet()
	{
		return this.recvFaucet;
	}
	
	public List<IAsyncTube<?,?,?,?>> getTubes()
	{
		return this.parts;
	}
	
	@SuppressWarnings("unchecked")
	public <T extends IAsyncTube<?, ?, ?, ?>> T getTube(int index)
	{
		return (T) this.parts.get(index);
	}
	
	public IAsyncSink<?,?> getSink()
	{
		return this.asyncSink;
	}

	@Override
	public Scheduler scheduler()
	{
		if (this.scheduler == null)
			this.scheduler = new TestScheduler(this::exceptionEncounteredNotifyListeners);
		return this.scheduler;
	}

}
