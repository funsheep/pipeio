package cc.renken.pipeio.sink.rs232;

/**
 * Possible stop bits definitions.
 */
public enum StopBits
{
	ONE,
	ONE_POINT_FIVE,
	TWO
}