/**
 * 
 */
package cc.renken.pipeio.sink.rs232.rxtx;

import java.io.IOException;
import java.util.TooManyListenersException;

import cc.renken.pipeio.IComponent;
import cc.renken.pipeio.IListener;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.sink.rs232.FlowControl;
import cc.renken.pipeio.sink.rs232.IRS232Port;
import cc.renken.pipeio.sink.rs232.Parity;
import cc.renken.pipeio.sink.rs232.RS232Sink;
import cc.renken.pipeio.sink.rs232.StopBits;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.RXTXPort;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

/**
 * @author renkenh
 *
 */
public class RXTXRS232Port implements IRS232Port
{

	private RXTXPort port;


	@Override
	public void connect(Configuration config, IListener listener) throws IOException
	{
		if (this.isConnected())
			throw new IllegalStateException("RS232Port is already opened.");
		try
		{
			CommPortIdentifier identifier = CommPortIdentifier.getPortIdentifier(config.getStringValue(RS232Sink.PROP_PORT));
			this.port = (RXTXPort) identifier.open("pipeio", config.getIntValue(IComponent.PROP_TIMEOUT));
			this.port.setSerialPortParams(config.getIntValue(RS232Sink.PROP_BAUD), config.getIntValue(RS232Sink.PROP_DATABITS), mapStopbits(config.getEnumEntry(RS232Sink.PROP_STOPBITS)), mapParity(config.getEnumEntry(RS232Sink.PROP_PARITY)));
			this.port.setFlowControlMode(mapFlowControl(config.getEnumEntry(RS232Sink.PROP_FLOWCONTROL)));
	
			//Seems as if this prevents the socket from being opened a second time
	//		serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING, timeout, timeout);
			this.port.addEventListener(new RXTXRS232Listener(this.port, listener));
		}
		catch (NoSuchPortException | PortInUseException | UnsupportedCommOperationException | TooManyListenersException ex)
		{
			this.port = null;
			throw new IOException(ex);
		}
	}

	@Override
	public boolean isConnected()
	{
		return this.port != null;
	}

	@Override
	public void push(byte[] data) throws IOException
	{
		if (!this.isConnected())
			throw new IOException("Port not connected.");
		this.port.getOutputStream().write(data);
	}

	@Override
	public void close() throws IOException
	{
		if (this.port != null)
		{
			this.port.removeEventListener();
			this.port.close();
		}
		this.port = null;
	}

	private static final int mapStopbits(StopBits bits)
	{
		switch (bits)
		{
			case ONE:
				return SerialPort.STOPBITS_1;
			case ONE_POINT_FIVE:
				return SerialPort.STOPBITS_1_5;
			case TWO:
				return SerialPort.STOPBITS_2;
			default:
				return -1;
		}
	}

	private static final int mapParity(Parity parity)
	{
		switch (parity)
		{
			case EVEN:
				return SerialPort.PARITY_EVEN;
			case MARK:
				return SerialPort.PARITY_MARK;
			case NONE:
				return SerialPort.PARITY_NONE;
			case ODD:
				return SerialPort.PARITY_ODD;
			case SPACE:
				return SerialPort.PARITY_SPACE;
			default:
				return -1;
		}
	}
	
	private static final int mapFlowControl(FlowControl control)
	{
		switch (control)
		{
			case NONE:
				return SerialPort.FLOWCONTROL_NONE;
			case RTSCTS:
				return SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT;
			case XONXOFF:
				return SerialPort.FLOWCONTROL_XONXOFF_IN | SerialPort.FLOWCONTROL_XONXOFF_OUT;
			default:
				return -1;
		}
	}
}
