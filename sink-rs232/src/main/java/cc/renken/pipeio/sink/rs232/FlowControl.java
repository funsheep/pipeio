package cc.renken.pipeio.sink.rs232;

/**
 * This enum represents every possible value for flowcontrol parameter.
 */
public enum FlowControl
{
	NONE,
	CTS,
	RTSCTS,
	DSR,
	DTRDSR,
	XONXOFF
}