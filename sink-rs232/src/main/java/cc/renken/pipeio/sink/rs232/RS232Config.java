/**
 * 
 */
package cc.renken.pipeio.sink.rs232;

/**
 * @author renkenh
 *
 */
public class RS232Config
{

	public final String portID;
	public final Baudrate baudrate;
	public final int dataBits;
	public final FlowControl flowControl;
	public final Parity parity;
	public final StopBits stopBits;
	public final int timeout;


	public RS232Config(String portID, Baudrate baudrate, int dataBits, FlowControl flowControl, Parity parity, StopBits stopBits, int timeout)
	{
		this.portID = portID;
		this.baudrate = baudrate;
		this.dataBits = dataBits;
		this.flowControl = flowControl;
		this.parity = parity;
		this.stopBits = stopBits;
		this.timeout = timeout;
	}

}
