package cc.renken.pipeio.sink.rs232;

/**
 * All allowed baud rates.
 */
public enum Baudrate
{
	R2400,
	R4800,
	R9600,
	R19200,
	R57600,
	R115200;
	
	public int getRate()
	{
		String rate = this.name().substring(1);
		return Integer.parseInt(rate);
	}
}