package cc.renken.pipeio.sink.rs232.rxtx;

import java.io.IOException;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.IListener.EventType;
import gnu.io.RXTXPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

final class RXTXRS232Listener implements SerialPortEventListener
{
	
	private static final int MAX_BUFFER_SIZE = 1024;

	private final RXTXPort port;
	private final IListener listener;
	private final byte[] readBuffer = new byte[MAX_BUFFER_SIZE];

	/**
	 * @param fcrs232Port
	 */
	public RXTXRS232Listener(RXTXPort port, IListener listener)
	{
		this.port = port;
		this.listener = listener;
	}


	@Override
	public void serialEvent(SerialPortEvent event)
	{
		if (event.getEventType() != SerialPortEvent.DATA_AVAILABLE)
			return;

		try
		{
			int read = -1;
			do
			{
				read = this.port.getInputStream().read(this.readBuffer);
				if (read > 0)
				{
					byte[] data = new byte[read];
					System.arraycopy(this.readBuffer, 0, data, 0, read);
					this.listener.handleEvent(EventType.DATA_RECEIVED, read);
				}
			}
			while (read == MAX_BUFFER_SIZE);
		}
		catch (IOException ex)
		{
			ex.printStackTrace();	//FIXME do nothing. This may happen because a timeout has occurred
		}
	}

}
