package cc.renken.pipeio.sink.rs232.fazecast;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.IListener.EventType;

final class FCRS232Listener implements SerialPortDataListener
{
	
	private static final int MAX_BUFFER_SIZE = 1024;

	private final SerialPort port;
	private final IListener listener;
	

	/**
	 * @param fcrs232Port
	 */
	public FCRS232Listener(SerialPort port, IListener listener)
	{
		this.port = port;
		this.listener = listener;
	}


	@Override
	public void serialEvent(SerialPortEvent event)
	{
		if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
			return;

		int toRead = Math.min(this.port.bytesAvailable(), MAX_BUFFER_SIZE);
		while (toRead > 0)
		{
			byte[] read = new byte[toRead];
			this.port.readBytes(read, toRead);
			this.listener.handleEvent(EventType.DATA_RECEIVED, read);

			toRead = Math.min(this.port.bytesAvailable(), MAX_BUFFER_SIZE);
		}
	}

	
	@Override
	public int getListeningEvents()
	{
		return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
	}
}
