/**
 * 
 */
package cc.renken.pipeio.sink.rs232;

import java.io.Closeable;
import java.io.IOException;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
public interface IRS232Port extends Closeable
{
	
	public void connect(Configuration config, IListener listener) throws IOException;

	public boolean isConnected();
	
	public void push(byte[] data) throws IOException;

}
