package cc.renken.pipeio.sink.rs232;

/**
 * Parity according to the RS232 specifications.
 */
public enum Parity
{
	NONE,
	ODD,
	EVEN,
	MARK,
	SPACE;
}