/**
 * 
 */
package cc.renken.pipeio.sink.rs232.fazecast;

import java.io.IOException;

import com.fazecast.jSerialComm.SerialPort;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.sink.rs232.FlowControl;
import cc.renken.pipeio.sink.rs232.IRS232Port;
import cc.renken.pipeio.sink.rs232.Parity;
import cc.renken.pipeio.sink.rs232.RS232Sink;
import cc.renken.pipeio.sink.rs232.StopBits;

/**
 * @author renkenh
 *
 */
public class FCRS232Port implements IRS232Port
{

	private SerialPort port;


	@Override
	public void connect(Configuration config, IListener listener) throws IOException
	{
		if (this.isConnected())
			throw new IllegalStateException("RS232Port is already opened.");
		
		this.port = SerialPort.getCommPort(config.getStringValue(RS232Sink.PROP_PORT));

		this.port.setComPortParameters(config.getIntValue(RS232Sink.PROP_BAUD), config.getIntValue(RS232Sink.PROP_DATABITS), mapStopbits(config.getEnumEntry(RS232Sink.PROP_STOPBITS)), mapParity(config.getEnumEntry(RS232Sink.PROP_PARITY)));
		this.port.setFlowControl(mapFlowControl(config.getEnumEntry(RS232Sink.PROP_FLOWCONTROL)));

		//Seems as if this prevents the socket from being opened a second time
//		serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING | SerialPort.TIMEOUT_WRITE_BLOCKING, timeout, timeout);
		this.port.addDataListener(new FCRS232Listener(this.port, listener));
	}

	@Override
	public boolean isConnected()
	{
		return this.port != null && this.port.isOpen();
	}

	@Override
	public void push(byte[] data) throws IOException
	{
		if (!this.isConnected())
			throw new IOException("Port is closed.");
		this.port.writeBytes(data, data.length);
	}

	@Override
	public void close() throws IOException
	{
		if (this.port != null)
		{
			this.port.removeDataListener();
			this.port.closePort();
		}
		this.port = null;
	}

	private static final int mapStopbits(StopBits bits)
	{
		switch (bits)
		{
			case ONE:
				return SerialPort.ONE_STOP_BIT;
			case ONE_POINT_FIVE:
				return SerialPort.ONE_POINT_FIVE_STOP_BITS;
			case TWO:
				return SerialPort.TWO_STOP_BITS;
			default:
				return -1;
		}
	}

	private static final int mapParity(Parity parity)
	{
		switch (parity)
		{
			case EVEN:
				return SerialPort.EVEN_PARITY;
			case MARK:
				return SerialPort.MARK_PARITY;
			case NONE:
				return SerialPort.NO_PARITY;
			case ODD:
				return SerialPort.ODD_PARITY;
			case SPACE:
				return SerialPort.SPACE_PARITY;
			default:
				return -1;
		}
	}
	
	private static final int mapFlowControl(FlowControl control)
	{
		switch (control)
		{
			case CTS:
				return SerialPort.FLOW_CONTROL_CTS_ENABLED;
			case DSR:
				return SerialPort.FLOW_CONTROL_DSR_ENABLED;
			case DTRDSR:
				return SerialPort.FLOW_CONTROL_DTR_ENABLED | SerialPort.FLOW_CONTROL_DSR_ENABLED;
			case NONE:
				return SerialPort.FLOW_CONTROL_DISABLED;
			case RTSCTS:
				return SerialPort.FLOW_CONTROL_RTS_ENABLED | SerialPort.FLOW_CONTROL_CTS_ENABLED;
			case XONXOFF:
				return SerialPort.FLOW_CONTROL_XONXOFF_IN_ENABLED | SerialPort.FLOW_CONTROL_XONXOFF_OUT_ENABLED;
			default:
				return -1;
		}
	}
}
