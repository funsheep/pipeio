/**
 * 
 */
package cc.renken.pipeio.sink.rs232;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.config.AtProperties.Property;
import cc.renken.pipeio.config.Configuration;
import cc.renken.pipeio.config.Validators;
import cc.renken.pipeio.impl.AAsyncSink;
import cc.renken.pipeio.sink.rs232.config.RS232PortValidator;


/**
 * @author renkenh
 *
 */
@Property(name=RS232Sink.PROP_PORT, isRequired=true, validationModel=RS232PortValidator.VALIDATOR_MODEL)
@Property(name=RS232Sink.PROP_BAUD, isRequired=true, validationModel="enum:cc.renken.pipeio.sink.rs232.Baudrate")
@Property(name=RS232Sink.PROP_DATABITS, isRequired=true, validationModel="int:0...32")
@Property(name=RS232Sink.PROP_FLOWCONTROL, isRequired=true, validationModel="enum:cc.renken.pipeio.sink.rs232.FlowControl")
@Property(name=RS232Sink.PROP_PARITY, isRequired=true, validationModel="enum:cc.renken.pipeio.sink.rs232.Parity")
@Property(name=RS232Sink.PROP_STOPBITS, isRequired=true, validationModel="enum:cc.renken.pipeio.sink.rs232.StopBits")
public class RS232Sink extends AAsyncSink<byte[], byte[]>
{
	
	public static final String PROP_PORT = "port";
	public static final String PROP_BAUD = "baudrate";
	public static final String PROP_DATABITS = "databits";
	public static final String PROP_FLOWCONTROL = "flowcontrol";
	public static final String PROP_PARITY = "parity";
	public static final String PROP_STOPBITS = "stopbits";


	static
	{
		Validators.registerValidator(RS232PortValidator.class);
	}

	private final IListener dataListener = (t,p) ->
	{
		this.handler().getScheduler().submit(() -> this.handler().pushToPrevious((byte[]) p));
	};
	private IRS232Port port;


	@Override
	protected void connect(Configuration configuration) throws IOException, TimeoutException
	{
		this.port = Utils.connectTo(configuration, this.dataListener);
	}


	@Override
	public void push(byte[] entity) throws IOException
	{
		try
		{
			this.port.push(entity);
		}
		catch (IOException e)
		{
			this.disconnect();
			throw e;
		}
	}

	@Override
	protected boolean isConnected()
	{
		return this.port != null && this.port.isConnected();
	}

	@Override
	protected void disconnect()
	{
		Utils.close(this.port);
		this.port = null;
		this.handler().notifyActiveStateChanged();
	}

}
