/**
 * 
 */
package cc.renken.pipeio.sink.rs232;

import java.io.Closeable;
import java.io.IOException;

import cc.renken.pipeio.IListener;
import cc.renken.pipeio.config.Configuration;

/**
 * @author renkenh
 *
 */
final class Utils
{

	/**
	 * Convenient method to create and connect a serial port. Parses the values given and initializes the port with these. 
	 * @param config The config from which to parse the parameters.
	 * @return An open Serial port.
	 * @throws IOException If the port could not be opened.
	 */
	static final IRS232Port connectTo(Configuration config, IListener listener) throws IOException
	{
		try
		{
			IRS232Port port = RS232Factory.createPort();
			port.connect(config, listener);
			return port;
		}
		catch (ClassNotFoundException e)
		{
			throw new IOException(e);
		}
	}

	static final void close(Closeable close)
	{
		try
		{
			if (close != null)
				close.close();
		}
		catch (IOException ex)
		{
			//die silently
		}
	}

	private Utils()
	{
		//no instance
	}
}
