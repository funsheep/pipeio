/**
 * 
 */
package cc.renken.pipeio.sink.rs232.config;

import java.util.Arrays;

import cc.renken.pipeio.config.AValidator;
import cc.renken.pipeio.config.IPropertyValidator.AtValidatorConfigs.Validator;
import cc.renken.pipeio.sink.rs232.RS232Factory;

/**
 * @author renkenh
 *
 */
@Validator(RS232PortValidator.VALIDATOR_MODEL)
public class RS232PortValidator extends AValidator<String>
{
	
	public static final String VALIDATOR_MODEL = "rs232port";
	
	public RS232PortValidator()
	{
		super(Arrays.asList(RS232Factory.getAvailablePorts()));
	}

	@Override
	public ValueType getValueType()
	{
		return ValueType.STRING;
	}

	@Override
	protected String convertSpec(String word)
	{
		return word;
	}

}
