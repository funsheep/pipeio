/**
 * 
 */
package cc.renken.pipeio.sink.rs232;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author renkenh
 *
 */
public final class RS232Factory
{
	
	private static final Logger logger = LoggerFactory.getLogger(RS232Factory.class);


	public enum Implementation
	{
		FAZECAST("com.fazecast.jSerialComm.SerialPort", "cc.renken.pipeio.async.sink.rs232.fazecast.FCRS232Port"),
		PUREJAVACOMM("purejavacomm.CommPortIdentifier", "cc.renken.pipeio.async.sink.rs232.purejavacomm.PJCRS232Port"),
		RXTX("gnu.io.CommPortIdentifier", "cc.renken.pipeio.async.sink.rs232.rxtx.RXTXRS232Port");
		
		private final String classToCheck;
		private final String classToLoad;
		
		private Implementation(String classToCheck, String classToLoad)
		{
			this.classToCheck = classToCheck;
			this.classToLoad  = classToLoad;
		}
	}
	
	
	private static IRS232ImplFactory FACTORY = null;
	
	
	public static final String[] getAvailablePorts()
	{
		try
		{
			findImplementation();
		}
		catch (ClassNotFoundException e)
		{
			return new String[0];
		}
		return FACTORY.getAvailablePorts();
	}
	
	public static final IRS232Port createPort() throws ClassNotFoundException
	{
		findImplementation();
		return FACTORY.createPort();
	}


	public static final synchronized void setCustomFactory(IRS232ImplFactory customFactory)
	{
		FACTORY = customFactory;
	}
	
	private static final synchronized void findImplementation() throws ClassNotFoundException
	{
		if (FACTORY != null)
			return;
		for (Implementation impl : Implementation.values())
		{
			try
			{
				IRS232ImplFactory factory = _loadImplementation(impl);
				setCustomFactory(factory);
			}
			catch (InstantiationException e)
			{
				//do nothing - just try next one
			}
		}
		throw new ClassNotFoundException("Could not find supported serial port library in class path.");
	}

	public static final synchronized void loadImplementation(Implementation impl) throws InstantiationException
	{
		IRS232ImplFactory factory = _loadImplementation(impl);
		setCustomFactory(factory);
	}

	private static final IRS232ImplFactory _loadImplementation(Implementation impl) throws InstantiationException
	{
		try
		{
			Class.forName(impl.classToCheck, false, RS232Factory.class.getClassLoader());
			return (IRS232ImplFactory) Class.forName(impl.classToLoad).getDeclaredConstructor().newInstance();
		}
		catch (Exception e)
		{
			logger.debug("Could not instantiate {}.", impl, e);
			throw new InstantiationException(impl.name());
		}
	}

	
	private RS232Factory()
	{
		//no instance
	}
}
