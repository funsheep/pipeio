/**
 * 
 */
package cc.renken.pipeio.sink.rs232;

/**
 * @author renkenh
 *
 */
public interface IRS232ImplFactory
{

	public String[] getAvailablePorts();
	
	public IRS232Port createPort();
}
